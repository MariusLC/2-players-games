#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
		///// STRATEGIES

		https://www.myriad-online.com/resources/docs/awale/francais/strategy.htm
		http://s.helan.free.fr/awale/conseils/
		http://abstractstrategygames.blogspot.com/2010/10/awale.html


"""
nbCoups = 4
largeur = 2
longueur = 6

def initJeu():
	global nbCoups
	nbCoups = 4
	return [initPlateau(), 1, None, [], initScore()]

def initPlateau():
	return [[4 for x in range(0,longueur,1)] for y in range(0,largeur,1)]
	
def initScore():
	return [0,0]
	
def finJeu(jeu):
	global nbCoups
	#print("finJeu, nbCoups :", nbCoups, " , score :", jeu[4])
	nbCoups += 1
	if jeu[4][0] > 24 or jeu[4][1] > 24:
		#print("Un des joueurs (", jeu[1], ") a mangé plus de la moitié des pions (son score est superieur a 24) il a gagné !")
		return True
	if nbCoups > 100:
		for i in jeu[0][0]:
			jeu[4][0] += i
		for j in jeu[0][1]:
			jeu[4][1] += j
		return True
	else:
		return False
	
def getCoupsValides(jeu):
	j = getJoueur(jeu)
	a = advEstAffame(jeu)
	if j == 1 :
		a =[ ((j-1), i) for i in range(6) if getCaseVal(jeu, j-1, i) > 0 and (not a) or nourrit(jeu,(j-1,i)) ]
	else :
		a =[ ((j-1), i) for i in range(5,-1,-1) if getCaseVal(jeu, j-1, i) > 0 and (not a) or nourrit(jeu,(j-1,i)) ]
	return a
	
def advEstAffame(jeu):
	j = getJoueur(jeu)
	adv = j % 2 + 1
	return sum(jeu[0][adv-1]) == 0

def neMangePas(jeu, pris, l, c):
	j = getJoueur(jeu)
	for i in range(len(pris)):
			l,c = nextCase(l,c)
			v = pris[-1-i]
			jeu[0][l][c] += v
			jeu[4][j-1] -= v
			
def distribue(jeu, case):
	v = getCaseVal(jeu, case[0], case[1])
	jeu[0][case[0]][case[1]] = 0
	nc = case
	while v > 0:
		nc = nextCase(nc[0], nc[1])
		if not nc == case:
			jeu[0][nc[0]][nc[1]] += 1
			v -= 1
	return nc
	
def nourrit(jeu, coup):
	j = getJoueur(jeu)
	if j == 1:
		return coup[1] < getCaseVal(jeu, coup[0], coup[1])
	return getCaseVal(jeu, coup[0], coup[1]) > 5-coup[1]
	
def joueCoup(jeu, coup):
	l,c = distribue(jeu,coup)
	v = getCaseVal(jeu,l,c)
	j = getJoueur(jeu)
	pris = []
	while (l==(j%2)) and ((v==2 or (v==3))):
		jeu[0][l][c] = 0
		jeu[4][j-1] += v
		l,c = nextCase(l,c,True)
		v = getCaseVal(jeu,l,c)
		pris.append(v)
	if advEstAffame(jeu):
		neMangePas(jeu, pris, l, c)
	# dans othello on a une fonction change score qui modifie le score après avoir joué le coup, en awele on le modifie directement pendant le coup
	jeu[3].append(coup)
    
def nextCase(l,c,horaire = False):
	if horaire:
		if c == 5 and l == 0:
			return(1,c)
		if c == 0 and l == 1:
			return(0,c)
		if l == 0:
			return(l,c+1)
		return (l, c-1)
	else :
		if c == 0 and l == 0:
			return(1,c)
		if c == 5 and l == 1:
			return(0,c)
		if l == 0:
			return(l,c-1)
		return (l, c+1)
		
def getCaseVal(jeu, ligne, colonne):
    return jeu[0][ligne][colonne]
   
def getJoueur(jeu):
    return jeu[1]
