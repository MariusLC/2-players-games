#!/usr/bin/env python
# -*- coding: utf-8 -*-
import awele
import sys
import time
import random
sys.path.append("..")
import game
game.game = awele
sys.path.append("./Joueurs")
import joueur_humain
import joueur_alphabeta
import joueur_alphabeta2
import joueur_aleatoire
import joueur_bagarre

game.joueur1 = joueur_aleatoire
game.joueur2 = joueur_aleatoire
joueur1 = "aleatoire"
joueur2 = "aleatoire"
hum = False


def selection(joueur):
	global hum
	if (joueur == "humain"):
		newjoueur = joueur_humain
		hum = True
		newjoueur = joueur_aleatoire
	elif (joueur == "alphabeta"):
		newjoueur = joueur_alphabeta
	elif (joueur == "alphabeta2"):
		newjoueur = joueur_alphabeta2
	elif (joueur == "bagarre"):
		newjoueur = joueur_bagarre

		
	else:
		print("\nErreur : Nom de joueur inconnu\n")
		exit()
	return newjoueur


nbParties = 100
lenArg = len(sys.argv)
if (lenArg == 1):
	nbApp = 1

elif (lenArg == 2):
	nbApp = int(sys.argv[1])

elif (lenArg == 3):
	nbApp = int(sys.argv[1])
	joueur1 = str(sys.argv[2])
	game.joueur1 = selection(joueur1)
	
elif (lenArg == 4):
	nbApp = int(sys.argv[1])
	joueur1 = str(sys.argv[2])
	joueur2 = str(sys.argv[3])
	game.joueur1 = selection(joueur1)
	game.joueur2 = selection(joueur2)


def playGame():
	vic = [0, 0, 0]
	for i in range(nbParties):
		jeu = game.initialiseJeu()

		if (nbParties > 1 and i == 0): 
			game.joueur2 = joueur_alphabeta2

		if (nbParties > 1 and i == nbParties//4):
			game.joueur1, game.joueur2 = game.joueur2, game.joueur1

		if (nbParties > 1 and i == nbParties//2):
			game.joueur1 = joueur_bagarre
			game.joueur1, game.joueur2 = game.joueur2, game.joueur1

		if (nbParties > 1 and i == 3*nbParties//4):
			game.joueur1, game.joueur2 = game.joueur2, game.joueur1

		if hum:
			game.affiche(jeu)

		if not hum:
			jtemp1 = game.joueur1
			jtemp2 = game.joueur2
			game.joueur1 = joueur_aleatoire
			game.joueur2 = joueur_aleatoire
			for j in range(4):
				coup = game.saisieCoup(jeu)
				game.joueCoup(jeu,coup)
			game.joueur1 = jtemp1
			game.joueur2 = jtemp2

		while not(game.finJeu(jeu)):
			copie = game.getCopieJeu(jeu)
			coup = game.saisieCoup(copie)
			game.joueCoup(jeu,coup)
			if hum:
				print("\nJoueur:", jeu[1])
				game.affiche(jeu)

		winner = game.getGagnant(jeu)
		# if (nbParties == 1 or i < nbParties//2):
		# 	vic[winner] += 1
		if (nbParties == 1 or (i < nbParties//4) or (i >= nbParties//2 and i < 3*nbParties//4)):
			vic[winner] += 1
		else:
			vic[winner%2+1] += 1

		# nombre de parties jouees
		string = str(i+1) + "/" + str(nbParties)
		# pourcentage de victoire qui s'incrmente
		string += "		" + str((int)((vic[1]*100)/(i+1))) + "%"
		print(string)

	vicJ1 = vic[1]
	vicJ2 = vic[2]
	game.joueur1, game.joueur2 = game.joueur2, game.joueur1
	return int(vicJ1 * 100 / (nbParties))
	
	
def addParam(phase, p, dep):
	phase[p] += dep

def write(nb, phase):
	string_fin = "phase" + nb + " = " + str(phase)
	print(string_fin)
	# creation du fichier
	filename = "Joueurs/coefficients" + nb + ".py"
	file = open(filename, "w")
	file.write(string_fin)
	file.close()

# choix composante
Eps = 0.1

phase1 = game.joueur1.phase1
phase2 = game.joueur1.phase2
phase3 = game.joueur1.phase3

# On décide de faire apprendre la phase :
phase = phase2
nb = "2"
# /////

string_debut = "\n--- DEBUT: " + str(phase)
print(string_debut)

result = playGame()
for a in range(nbApp):
	string_app = "\nAPPRENTISSAGE " + str(a+1) + "/" + str(nbApp) + "\n"
	print(string_app)

	p = random.randint(0,len(phase)-1)
	dep = Eps
	x = random.random()
	print("\nx:", x, "\n")
	if x < 0.5:
		dep = -Eps

	addParam(phase, p, dep)
	print("On teste ce changement sur le parametre ", p ," : ", phase)
	result2 = playGame()

	print("\n" + str(result) + " -> " + str(result2) + "\n")
	if result > result2:
		addParam(phase, p, -dep)
	else:
		string_phase = "Phase changée : " + str(phase) + "\n"
		print(string_phase)
		result = result2
	Eps *= 0.99

	write(nb, phase)

print()

# append au fichier de sauvegarde
filename_bu = "Joueurs/coefficients_backup.py"
file_bu = open(filename_bu, "a")
file_bu.write(string_fin + "\n")
file_bu.close()
