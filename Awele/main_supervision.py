#!/usr/bin/env python
# -*- coding: utf-8 -*-
import awele
import sys
import time
import random
sys.path.append("..")
import game
game.game = awele
sys.path.append("./Joueurs")
import joueur_humain
import joueur_aleatoire
import joueur_alphabeta
import joueur_site
import joueur_opti
import joueur_board
import joueur_super
import joueur_score
import NastyBanana
import BananusIII

game.joueur1 = joueur_super
game.joueur2 = joueur_aleatoire
joueur1 = "super"
joueur2 = "aleatoire"
hum = False


def selection(joueur):
	global hum
	if (joueur == "humain"):
		newjoueur = joueur_humain
		hum = True
	elif (joueur == "aleatoire"):
		newjoueur = joueur_aleatoire
	elif (joueur == "alphabeta"):
		newjoueur = joueur_alphabeta
	elif (joueur == "site"):
		newjoueur = joueur_site
	elif (joueur == "opti"):
		newjoueur = joueur_opti
	elif (joueur == "board"):
		newjoueur = joueur_board
	elif (joueur == "super"):
		newjoueur = joueur_super
	elif (joueur == "score"):
		newjoueur = joueur_score
	elif (joueur == "NastyBanana"):
		newjoueur = NastyBanana
	elif (joueur == "BananusIII"):
		newjoueur = BananusIII

	else:
		print("\nErreur : Nom de joueur inconnu\n")
		exit()
	return newjoueur


nbParties = 100
lenArg = len(sys.argv)
if (lenArg == 1):
	nbApp = 1

elif (lenArg == 2):
	nbApp = int(sys.argv[1])

elif (lenArg == 3):
	nbApp = int(sys.argv[1])
	joueur1 = str(sys.argv[2])
	game.joueur1 = selection(joueur1)

elif (lenArg == 4):
	nbApp = int(sys.argv[1])
	joueur1 = str(sys.argv[2])
	joueur2 = str(sys.argv[3])
	game.joueur1 = selection(joueur1)
	game.joueur2 = selection(joueur2)


def playGame():
	vic = [0, 0, 0]
	for i in range(nbParties):
		jeu = game.initialiseJeu()

		if (nbParties > 1 and i == nbParties//2):
			game.joueur1, game.joueur2 = game.joueur2, game.joueur1

		if hum:
			game.affiche(jeu)

		if not hum:
			jtemp1 = game.joueur1
			jtemp2 = game.joueur2
			game.joueur1 = joueur_aleatoire
			game.joueur2 = joueur_aleatoire
			for j in range(4):
				coup = game.saisieCoup(jeu)
				game.joueCoup(jeu,coup)
			game.joueur1 = jtemp1
			game.joueur2 = jtemp2

		while not(game.finJeu(jeu)):
			copie = game.getCopieJeu(jeu)
			coup = game.saisieCoup(copie)
			game.joueCoup(jeu,coup)
			if hum:
				print("\nJoueur:", jeu[1])
				game.affiche(jeu)

		if (nbParties == 1 or i < nbParties//2):
			vic[game.getGagnant(jeu)] += 1
		else:
			vic[game.getGagnant(jeu)%2+1] += 1

		# nombre de parties jouees
		string = str(i+1) + "/" + str(nbParties)
		# pourcentage de victoire qui s'incrmente
		string += "\t" + str(int((vic[1]*100)/(i+1))) + " %"
		print(string)

	game.joueur1, game.joueur2 = game.joueur2, game.joueur1
	return int(vic[1] * 100 / (nbParties))


def addParam(phase, p, dep):
	phase[p] += dep


# Debut de l apprentissage #
t = time.time();
# choix composante
Eps = 1

phase1 = game.joueur1.phase_eleve1
phase2 = game.joueur1.phase_eleve2
phase3 = game.joueur1.phase_eleve3
phaseList = [phase1, phase2, phase3]


string_debut = "\n --- DEBUT: " + str(phaseList)
print(string_debut)

result = playGame()

total_coups = game.joueur1.TOTAL_COUPS
total_bon_choix = game.joueur1.TOTAL_BON_CHOIX
print("\n\n\nTOTAUX: ", total_coups, total_bon_choix)
game.joueur1.TOTAL_COUPS = 0
game.joueur1.TOTAL_BON_CHOIX = 0

result = 100 * total_bon_choix / total_coups
# On compte le nb de victoire d'affilées d'une phase donnée, s'il fait 10 win, on recalcule son % de vic.
win = 0
for a in range(nbApp):	

	print("\n -- APPRENTISSAGE " + str(a+1) + "/" + str(nbApp))
	# Selection aleatoire de la phase
	# i = random.randint(0,2)
	# On fait une seule phase pour l'instant
	i = 1
	phase = phaseList[i]
	print("\n - PHASE " + str(i+1) + "/" + str(len(phaseList)))

	p = random.randint(0,len(phase)-1)
	print("\n - COEFFICIENT " + str(p+1) + "/" + str(len(phase)) + "\n")

	dep = Eps
	x = random.random()
	if x < 0.5:
		dep *= -1

	addParam(phase, p, dep)
	playGame()

	total_coups = game.joueur1.TOTAL_COUPS
	total_bon_choix = game.joueur1.TOTAL_BON_CHOIX
	print("\n\n\nTOTAUX: ", total_coups, total_bon_choix)
	game.joueur1.TOTAL_COUPS = 0
	game.joueur1.TOTAL_BON_CHOIX = 0

	result2 = 100 * total_bon_choix / total_coups

	print("\n" + str(result) + " -> " + str(result2) + "\n")
	if result > result2:
		win += 1
		addParam(phase, p, -dep)
	else:
		result = result2
		print("PHASE: " + str(phase) + "\n")

		str1 = "phase1 = " + str(phaseList[0])
		str2 = "phase2 = " + str(phaseList[1])
		str3 = "phase3 = " + str(phaseList[2])

		# string_fin = "phase" + i + " = " + str(phase)
		# creation du fichier
		filename = "Joueurs/coefficients_super_eleve.py"
		file = open(filename, "w")
		file.write(str1 + "\n" + str2 + "\n" + str3)
		file.close()

	#Eps *= 0.999
	# uniquement si on test plusieurs fois le même coefficient

	# On compte le nb de victoire d'affilées d'une phase donnée, s'il fait 10 win, on recalcule son % de vic.
	if win == 10:
		win = 0
		playGame()

		total_coups = game.joueur1.TOTAL_COUPS
		total_bon_choix = game.joueur1.TOTAL_BON_CHOIX
		print("\n\n\nWIN10 TOTAUX: ", total_coups, total_bon_choix)
		game.joueur1.TOTAL_COUPS = 0
		game.joueur1.TOTAL_BON_CHOIX = 0
		
	if (result2 == 100):
		print("BREAK HERE")

print("PHASE: " + str(phase) + "\n")
print("\n" + str(a+1) + " apprentissages")
temps = time.time() - t
print("\nTemps d'exécution: " + str(round(temps)) + "s")
print()

# append au fichier de sauvegarde
filename_bu = "Joueurs/coefficients_super_eleve.py"
file_bu = open(filename_bu, "a")
file_bu.write(str1 + "\n" + str2 + "\n" + str3)
file_bu.close()
