#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
import random
import time
import math
infinity = math.inf

global moi
global profondeur
global nb
nb = 0
profondeur = 3
totaltime = 0
globaltime = 0

#finjeu = game.finJeu
getgagnant = game.getGagnant
getcoups = game.getCoupsValides
changeJ = game.changeJoueur
getJ = game.getJoueur
copie = game.getCopieJeu
joueCoup = game.joueCoup

def saisieCoup(jeu):
	global moi#, totaltime, globaltime
	#print("Avant saisieCoup")
	#game.affiche(jeu)
	moi = getJ(jeu)
	# totaltime = 0

	listeCoupsValides = getcoups(jeu)
	bestCoup = listeCoupsValides[0]
	alpha = -infinity
	beta = infinity
	note = -infinity


	for coup in listeCoupsValides:
		prevision = copie(jeu)
		joueCoup(prevision, coup)
		#false = min
		note = estimation(prevision, False, alpha, beta) #MinOuMax bien  egal à False car ce sont des noeud adverse (une bouble deja dans decision)
		if note > alpha :
			alpha = note
			bestCoup = coup
	# print("STOTALTIME:", totaltime)
	# globaltime += totaltime
	# print("-- SGLOBALTIME:", globaltime)
	return bestCoup


def estimation(jeu, MinOuMax, alpha, beta, p = 1):
	# global totaltime

	if finJeu(jeu) : 

		g = getgagnant(jeu)
		if g == moi :
			return 1000
		elif g == moi % 2 + 1 :
			return -1000
		else:
			return -100

	if p >= profondeur :
		return fonctionEvaluation(jeu)
	listeCoupsValides = getcoups(jeu)
	if MinOuMax:
		#MAX
		note = -infinity
		for coup in listeCoupsValides:
			# debut = time.time()
			# prevision = copie(jeu)
			# totaltime += time.time() - debut
			prevision = copie(jeu)
			joueCoup(prevision, coup)
			note = max(note, estimation(prevision, not MinOuMax, alpha, beta, p+1))
			# debut = time.time()
			# totaltime += time.time() - debut
			if note >= beta :
				return note
			alpha = max(alpha, note)
		return note
	else:
		#MIN
		note = infinity
		for coup in listeCoupsValides:
			# debut = time.time()
			# prevision = copie(jeu)
			# totaltime += time.time() - debut
			prevision = copie(jeu)
			joueCoup(prevision, coup)
			note = min(note, estimation(prevision, not MinOuMax, alpha, beta, p+1))
			# debut = time.time()
			# totaltime += time.time() - debut
			if note <= alpha:
				return note
			beta = min(beta, note)
		return note


def fonctionEvaluation(jeu):
	return score(jeu)
		
def score(jeu):
	score = jeu[4][moi-1] - jeu[4][moi % 2]
	#On ramene le score entre 0 et 1
	return intervalle(score, 40)

def intervalle(score, total):
	score = (score / total ) + 0.5
	if score > 1 :
		score = 1
	if score < 0 :
		score = 0
	return score

def finJeu(jeu):
	if jeu[4][0] > 24 or jeu[4][1] > 24:
		#print("Un des joueurs (", jeu[1], ") a mangé plus de la moitié des pions (son score est superieur a 24) il a gagné !")
		return True
	else:
		return False