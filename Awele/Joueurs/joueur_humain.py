#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game

def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    print("\nSaisie coup:")
    print("liste coups valides : ", jeu[2])
    while True:
    	try :
    		coupJ = int(input("Numero de la case = "))
    		if coupJ > 5 or coupJ < 0:
    			raise ValueError
    		break
    	except ValueError:
    		print("Oops! choisi un chiffre entre 0 et 5")
    		
    return (jeu[1]-1, coupJ)
    
    
