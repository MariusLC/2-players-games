#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
import random
import time
import math
infinity = math.inf

import coefficients_super_maitre
import coefficients_board
import coefficients_alphabeta

# au cas ou le fichier n'est pas au bon format on attribue une valeur par defaut
try:
	phase1 = coefficients_alphabeta.phase1
except AttributeError:
	phase1 = [1, 1, 1, 1, 1, 1, 1, 1, 1]

try:
	phase2 = coefficients_alphabeta.phase2
except AttributeError:
	phase2 = phase1

try:
	phase3 = coefficients_alphabeta.phase3
except AttributeError:
	phase3 = phase1

global moi
coeffs = [phase1, phase2, phase3]
nbCoeffs = len(phase1)
global profondeur 
profondeur = 3
#totaltime = 0
#globaltime = 0

#finjeu = game.finJeu
getgagnant = game.getGagnant
getcoups = game.getCoupsValides
changeJ = game.changeJoueur
copie = game.getCopieJeu
joueCoup = game.joueCoup

def saisieCoup(jeu):
	global moi#, totaltime, globaltime
	#print("Avant saisieCoup")
	#game.affiche(jeu)
	moi = game.getJoueur(jeu)
	#totaltime = 0

	listeCoupsValides = getcoups(jeu)
	bestCoup = listeCoupsValides[0]
	alpha = -infinity
	beta = infinity
	note = -infinity


	for coup in listeCoupsValides:
		prevision = copie(jeu)
		joueCoup(prevision, coup)
		#false = min
		note = estimation(prevision, False, alpha, beta) #MinOuMax bien  egal à False car ce sont des noeud adverse (une bouble deja dans decision)
		if note > alpha :
			alpha = note
			bestCoup = coup
	#print("TOTALTIME:", totaltime)
	#globaltime += totaltime
	#print("-- GLOBALTIME:", globaltime)
	return bestCoup


def estimation(jeu, MinOuMax, alpha, beta, p = 1):
	#global totaltime

	if finjeu(jeu) : 

		g = getgagnant(jeu)
		if g == moi :
			return 1000
		elif g == moi % 2 + 1 :
			return -1000
		else:
			return -100

	if p >= profondeur :
		return fonctionEvaluation(jeu)
	listeCoupsValides = getcoups(jeu)
	if MinOuMax:
		#MAX
		note = -infinity
		for coup in listeCoupsValides:
			#debut = time.time()
			#prevision = copie(jeu)
			#totaltime += time.time() - debut
			prevision = copie(jeu)
			joueCoup(prevision, coup)
			note = max(note, estimation(prevision, not MinOuMax, alpha, beta, p+1))
			#debut = time.time()
			#totaltime += time.time() - debut
			if note >= beta :
				return note
			alpha = max(alpha, note)
		return note
	else:
		#MIN
		note = infinity
		for coup in listeCoupsValides:
			#debut = time.time()
			#prevision = copie(jeu)
			#totaltime += time.time() - debut
			prevision = copie(jeu)
			joueCoup(prevision, coup)
			note = min(note, estimation(prevision, not MinOuMax, alpha, beta, p+1))
			#debut = time.time()
			#totaltime += time.time() - debut
			if note <= alpha:
				return note
			beta = min(beta, note)
		return note


def fonctionEvaluation(jeu):
	res = 0
	plat = [plateau(jeu, moi), plateau(jeu, moi%2+1)]
	cp = [coupsValides(jeu, moi), coupsValides(jeu, moi%2+1)]
	pui = [puit(jeu,moi), puit(jeu,moi%2+1)]
	vul = [vulnerable(jeu, moi), vulnerable(jeu, moi%2+1)]
	fctsEval = [score(jeu), pui[0], pui[1], vul[0], vul[1], plat[0], plat[1], cp[0], cp[1]]
	"""game.affiche(jeu)
	print("moi : ", moi)"""
	#print(fctsEval)

	#print("score : ", jeu[4])
	# //////////////////// Séparer quand on gagne et quand l'adversaire gagne  ????
	if (jeu[4][0] <= 6) and (jeu[4][1] <= 6) :
		phase = phase1
	elif (jeu[4][0] <= 17) and (jeu[4][1] <= 17) :
		phase = phase2
	else :
		phase = phase3
	
	if nbCoeffs != len(fctsEval):
		print("erreur : Pas le meme nb coeffs dans phase et fctsEval")

	# phase[0] * fctsEval[0] + phase[1] * fctsEval[1] + phase[2] * fctsEval[2]
	for i in range(len(phase)):
		res += fctsEval[i] * phase[i]
	return res

def finjeu(jeu):
	if jeu[4][0] > 24 or jeu[4][1] > 24:
		#print("Un des joueurs (", jeu[1], ") a mangé plus de la moitié des pions (son score est superieur a 24) il a gagné !")
		return True
	else:
		return False

def getCaseVal(jeu, ligne, colonne):
    return jeu[0][ligne][colonne]
   
def getJoueur(jeu):
    return jeu[1]	
		
def score(jeu):
	#print("Note score = ", jeu[4][moi-1] - jeu[4][moi % 2])
	score = jeu[4][moi-1] - jeu[4][moi % 2]
	#On ramene le score entre 0 et 1
	return intervalle(score, 40)

def plateau(jeu, joueur):
	# JE CONSIDERE QUE NOUS EVALUONS LA NOTE DU JOUEUR DU HAUT
	Map = [[10,  8,  4,  2,  1,  1],
		   [1,  1,  2,  4,  8,  10]]
	note = 0
	i2 = 0
	for i in jeu[0][joueur-1]:
		note += Map[joueur-1][i2] * getCaseVal(jeu, joueur-1, i2)
		i2 += 1
	return intervalle(note, 200)

def deltaPlateau(jeu):
	p = plateau(jeu)
	#On ramene le score entre 0 et 1
	return intervalle(p[moi]-p[moi%2+1], 400)

def coupsValides(jeu, joueur):
	j = jeu[1]
	jeu[1] = joueur
	score = len(getcoups(jeu))
	jeu[1] = joueur
	return intervalle(score, 5)

# fonction qui permet de departeger deux coups qui ont la même note mais pas très impactante.
def deltaCoupsValides(jeu, j):
	joueur = jeu[1]

	jeu[1] = moi
	scoreMoi = len(getcoups(jeu))

	jeu[1] = moi % 2 + 1
	scoreAdv = len(getcoups(jeu))

	jeu[1] = joueur
	score = scoreMoi - scoreAdv
	return intervalle(score, 10)

# Fonction qui calcule le nb de cases ayant 1 ou 2 pions qui se suivent
def vulnerable(jeu, joueur):
	pris = []
	score = 0
	if (joueur-1) == 1:
		for i in range(0,6,1):
			v = getCaseVal(jeu,0, i)
			if ((v==2) or (v==3)):
				score += v
			else :
				pris.append(score)
				score = 0

	else :
		for i in range(5,-1,-1):
			v = getCaseVal(jeu,1, i)
			if ((v==2) or (v==3)):
				score += v
			else :
				pris.append(score)
				score = 0
	if pris != []:
		return intervalle(max(pris), 10)
	return 0

def deltaVulnerable(jeu):
	score = vulnerable(jeu, moi) - vulnerable(jeu, moi%2+1)
	return intervalle(score, 20)

def puit(jeu, joueur) :
	best = 0
	for i in range(6):
		v = getCaseVal(jeu,joueur-1, i)
		best = max(best, v)
	return intervalle(best, 10)

def deltaPuit(jeu):
	score = puit(jeu, moi) - puit(jeu, moi%2+1)
	res = intervalle(score, 20)
	return res

def intervalle(score, total):
	score = (score / total ) + 0.5
	if score > 1 :
		score = 1
	if score < 0 :
		score = 0
	return score