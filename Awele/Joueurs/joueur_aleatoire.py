#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
import random
import time

global moi
global profondeur

def saisieCoup(jeu):

	liste = game.getCoupsValides(jeu)
	l = len(liste)
	coup = liste[random.randint(0,l-1)]

	return coup