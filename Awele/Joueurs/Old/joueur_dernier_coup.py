#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
import random

def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    liste = game.getCoupsValides(jeu)
    l = len(liste)
    coup = liste[-1]
    return coup
