Actuellement pour chaque coup nous avons un temps total de copiejeu entre 0.001 et 0.3 secondes
Soit pour 10 parties 21s de copie sur 42s de jeu total (50%)

On tente de remplacer copiejeu par annulecoup:

Temps d'execution d'annulecoup pour les 50 parties: 0.24s

Nombre de parties: 50
Matchs nuls: 1
Victoires joueur opti : 37 (74%)
Victoires joueur aleatoire : 12 (24%)

Temps d'exécution: 44s

////////
python3 main.py 1000 horizon1 horizon1_1

coin *10:
Nombre de parties: 1000
Matchs nuls: 41
Victoires joueur 1 (horizon1): 288
Victoires joueur 2 (horizon1_1): 671
Pourcentage de victoires du joueur 1: 28.8%


coin *5:
Nombre de parties: 1000
Matchs nuls: 48
Victoires joueur 1 (horizon1): 274
Victoires joueur 2 (horizon1_1): 678
Pourcentage de victoires du joueur 1: 27.4%


coin *50:
Nombre de parties: 1000
Matchs nuls: 57
Victoires joueur 1 (horizon1): 261
Victoires joueur 2 (horizon1_1): 682
Pourcentage de victoires du joueur 1: 26.1%

coin *20:
Nombre de parties: 1000
Matchs nuls: 40
Victoires joueur 1 (horizon1): 253
Victoires joueur 2 (horizon1_1): 707
Pourcentage de victoires du joueur 1: 25.3%
