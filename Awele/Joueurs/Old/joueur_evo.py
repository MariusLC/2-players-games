#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
import random
import time
import math
infinity = math.inf

# import coefficients_opti

# # au cas ou le fichier n'est pas au bon format on attribue une valeur par defaut
# try:
# 	phase1 = coefficients_opti.phase1
# except AttributeError:
# 	phase1 = [1, 1, 1, 1, 1, 1, 1, 1, 1]

global moi
# phase1 = [12, -2, -6, -1, -1, -3, -1, 1, 0]
phase1 = [14, 3, -2, 0, 0.6, 2, 0, 2, 0]
phase2 = phase1
phase3 = phase1
coeffs = [phase1, phase2, phase3]
global profondeur 
profondeur = 3

getgagnant = game.getGagnant
getcoups = game.getCoupsValides
changeJ = game.changeJoueur
copie = game.getCopieJeu
joueCoup = game.joueCoup

def saisieCoup(jeu):
	global moi
	moi = game.getJoueur(jeu)

	listeCoupsValides = getcoups(jeu)
	bestCoup = listeCoupsValides[0]
	alpha = -infinity
	beta = infinity
	note = -infinity

	for coup in listeCoupsValides:
		prevision = copie(jeu)
		joueCoup(prevision, coup)
		# false = min
		note = estimation(prevision, False, alpha, beta) #MinOuMax bien  egal à False car ce sont des noeud adverse (une bouble deja dans decision)
		if note > alpha :
			alpha = note
			bestCoup = coup
	return bestCoup


def estimation(jeu, MinOuMax, alpha, beta, p = 1):
	if finjeu(jeu) : 
		g = getgagnant(jeu)
		if g == moi :
			return 1000
		elif g == moi % 2 + 1 :
			return -1000
		else:
			return -100

	if p >= profondeur :
		return fonctionEvaluation(jeu)
	listeCoupsValides = getcoups(jeu)
	if MinOuMax:
		#MAX
		note = -infinity
		for coup in listeCoupsValides:
			prevision = copie(jeu)
			joueCoup(prevision, coup)
			note = max(note, estimation(prevision, not MinOuMax, alpha, beta, p+1))
			if note >= beta :
				return note
			alpha = max(alpha, note)
		return note
	else:
		#MIN
		note = infinity
		for coup in listeCoupsValides:
			prevision = copie(jeu)
			joueCoup(prevision, coup)
			note = min(note, estimation(prevision, not MinOuMax, alpha, beta, p+1))
			if note <= alpha:
				return note
			beta = min(beta, note)
		return note


def fonctionEvaluation(jeu):
	res = 0
	pui = [puit(jeu,moi),puit(jeu,moi%2+1)]
	vul = [vulnerable(jeu, moi), vulnerable(jeu, moi%2+1)]
	plat = plateau(jeu)
	cp = deltaCoupsValides(jeu)
	#print(plat,cp,pui,vul)
	fctsEval = [score(jeu), intervalle(pui[0], 20), intervalle(pui[1], 20), intervalle(vul[0], 10), intervalle(vul[1], 10), intervalle(plat[0], 200), intervalle(plat[1], 200), intervalle(cp[0], 5), intervalle(cp[1], 5)]

	nbPions = jeu[4][0] + jeu[4][1]
	if nbPions <= 12:
		phase = phase1
	elif nbPions <= 48 : 
		phase = phase2
	else : 
		phase = phase3
	
	# phase[0] * fctsEval[0] + phase[1] * fctsEval[1] + phase[2] * fctsEval[2]
	for p, e in zip(phase, fctsEval):
		res += p * e
	return res

def finjeu(jeu):
	if jeu[4][0] > 24 or jeu[4][1] > 24:
		#print("Un des joueurs (", jeu[1], ") a mangé plus de la moitié des pions (son score est superieur a 24) il a gagné !")
		return True
	else:
		return False

def getCaseVal(jeu, ligne, colonne):
    return jeu[0][ligne][colonne]
   
def getJoueur(jeu):
    return jeu[1]	
		
def score(jeu):
	score = jeu[4][moi-1] - jeu[4][moi % 2]
	return intervalle(score, 40)

def plateau(jeu):
	# JE CONSIDERE QUE NOUS EVALUONS LA NOTE DU JOUEUR DU HAUT
	Map=[[10,  8,  4,  2,  1,  1],
		 [1,  1,  2,  4,  8,  10]]

	noteJeu = 0
	noteJeuAdv = 0
	i2 = 0
	for i in jeu[0][0]:
		noteJeu += Map[0][i2] * getCaseVal(jeu, 0, i2)
		i2 += 1
	i2 = 0
	for i in jeu[0][1]:
		noteJeuAdv += Map[0][i2] * getCaseVal(jeu, 1, i2)
		i2 += 1
	return [noteJeu, noteJeuAdv]

def deltaPlateau(jeu):
	p = plateau(jeu)
	return intervalle(p[0]-p[1], 400)


# fonction qui permet de departeger deux coups qui ont la même note mais pas très impactante.
def deltaCoupsValides(jeu):
	joueur = jeu[1]
	jeu[1] = moi
	coupsMoi = len(getcoups(jeu))
	jeu[1] = moi % 2 + 1
	coupsAdv = len(getcoups(jeu))
	jeu[1] = joueur

	return [coupsMoi, coupsAdv]

# Fonction qui calcule le nb de cases ayant 1 ou 2 pions qui se suivent
def vulnerable(jeu, joueur):
	pris = []
	score = 0
	if (joueur-1) == 1:
		for i in range(0,6,1):
			v = getCaseVal(jeu,0, i)
			if ((v==2) or (v==3)):
				score += v
			else :
				pris.append(score)
				score = 0
	else :
		for i in range(5,-1,-1):
			v = getCaseVal(jeu,1, i)
			if ((v==2) or (v==3)):
				score += v
			else :
				pris.append(score)
				score = 0
	if pris != []:
		return max(pris)
	return 0

def deltaVulnerable(jeu):
	score = vulnerable(jeu, moi) - vulnerable(jeu, moi%2+1)
	return intervalle(score, 20)

def puit(jeu, joueur) :
	best = 0
	for i in range(6):
		v = getCaseVal(jeu,joueur-1, i)
		best = max(best, v)
	return best

def deltaPuit(jeu):
	score = puit(jeu, moi) - puit(jeu, moi%2+1)
	res = intervalle(score, 20)
	return res

def intervalle(score, total):
	score = (score / total ) + 0.5
	if score > 1 :
		score = 1
	if score < 0 :
		score = 0
	return score