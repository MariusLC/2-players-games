#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
import random
import time
import math
infinity = math.inf

import coefficients
# au cas ou le fichier n'est pas au bon format on attribue une valeur par defaut
try:
	phase1 = coefficients.phase1
except AttributeError:
	phase1 = [1, 1, 1, 1, 1]

global moi
#phase1 = [1, 1, 1, 1, 1]
phase2 = [1, 1, 1, 1, 1]
phase3 = [1, 1, 1, 1, 1]
coeffs = [phase1, phase2, phase3]
global profondeur 
profondeur = 3
#totaltime = 0
#globaltime = 0

#finjeu = game.finJeu
getgagnant = game.getGagnant
getcoups = game.getCoupsValides
changeJ = game.changeJoueur
copie = game.getCopieJeu
joueCoup = game.joueCoup


def saisieCoup(jeu):
	global nbCoups
	global moi#, totaltime, globaltime
	moi = game.getJoueur(jeu)
	#totaltime = 0

	listeCoupsValides = getcoups(jeu)
	bestCoup = listeCoupsValides[0]
	alpha = -infinity
	beta = infinity
	note = -infinity


	for coup in listeCoupsValides:
		print("Avant joueCoup")
		game.affiche(jeu)
		prevision = copie(jeu)
		joueCoup(prevision, coup)
		#false = min
		print("Apres joueCoup")
		game.affiche(prevision)
		note = estimation(prevision, False, alpha, beta) #MinOuMax bien  egal à False car ce sont des noeud adverse (une bouble deja dans decision)
		if note > alpha :
			alpha = note
			bestCoup = coup
	#print("TOTALTIME:", totaltime)
	#globaltime += totaltime
	#print("-- GLOBALTIME:", globaltime)
	return bestCoup


def estimation(jeu, MinOuMax, alpha, beta, p = 1):
	#global totaltime

	if finjeu(jeu) : 
	# probleme avec finjeu : on n'annule pas le changement de score en cas de nbcoups > 100...
	# autre probleme : on ajoute à nbCoups à chaque fois que l'on teste un jeu

		g = getgagnant(jeu)
		if g == moi:
			return 1000
		elif g == moi % 2 + 1 :
			return -1000
		else:
			return -100

	if p >= profondeur :
		return fonctionEvaluation(jeu)
	listeCoupsValides = getcoups(jeu)
	if MinOuMax:
		#MAX
		note = -infinity
		for coup in listeCoupsValides:
			#debut = time.time()
			#prevision = copie(jeu)
			#totaltime += time.time() - debut
			print("Avant joueCoup")
			game.affiche(jeu)
			prevision = copie(jeu)
			joueCoup(prevision, coup)
			print("Apres joueCoup")
			game.affiche(prevision)
			note = max(note, estimation(prevision, not MinOuMax, alpha, beta, p+1))
			#debut = time.time()
			#totaltime += time.time() - debut
			if note >= beta :
				return note
			alpha = max(alpha, note)
		return note
	else:
		#MIN
		note = infinity
		for coup in listeCoupsValides:
			#debut = time.time()
			#prevision = copie(jeu)
			#totaltime += time.time() - debut
			print("Avant joueCoup")
			game.affiche(jeu)
			prevision = copie(jeu)
			joueCoup(prevision, coup)
			print("Apres joueCoup")
			game.affiche(prevision)
			note = min(note, estimation(prevision, not MinOuMax, alpha, beta, p+1))
			#debut = time.time()
			#totaltime += time.time() - debut
			if note <= alpha:
				return note
			beta = min(beta, note)
		return note


def fonctionEvaluation(jeu):
	res = 0
	fctsEval = [deltaPlateau(jeu), score(jeu), deltaCoupsValides(jeu)]
	print(fctsEval)
	nbPions = jeu[4][0] + jeu[4][1]
	if nbPions <= 12:
		phase = phase1
	elif nbPions <= 48 : 
		phase = phase2
	else : 
		phase = phase3
	
	# phase[0] * fctsEval[0] + phase[1] * fctsEval[1] + phase[2] * fctsEval[2]
	for i in phase:
		for j in fctsEval:
			res += i * j

	return res

def finjeu(jeu):
	if jeu[4][0] > 24 or jeu[4][1] > 24:
		#print("Un des joueurs (", jeu[1], ") a mangé plus de la moitié des pions (son score est superieur a 24) il a gagné !")
		return True
	else:
		return False

def getCaseVal(jeu, ligne, colonne):
    return jeu[0][ligne][colonne]
   
def getJoueur(jeu):
    return jeu[1]	
		
def score(jeu):
	#print("Note score = ", jeu[4][moi-1] - jeu[4][moi % 2])
	return jeu[4][moi-1] - jeu[4][moi % 2]

def plateau(jeu):
	# JE CONSIDERE QUE NOUS EVALUONS LA NOTE DU JOUEUR DU HAUT
	Map=[[10,  8,  4,  2,  1,  1],
		 [1,  1,  2,  4,  8,  10]]

	noteJeu = 0
	noteJeuAdv = 0
	i2 = 0
	for i in jeu[0][0]:
		noteJeu += Map[0][i2] * getCaseVal(jeu, 0, i2)
		i2 += 1
	i2 = 0
	for i in jeu[0][1]:
		noteJeuAdv += Map[0][i2] * getCaseVal(jeu, 1, i2)
		i2 += 1
	return [noteJeu, noteJeuAdv]

def deltaPlateau(jeu):
	p = plateau(jeu)
	return p[0] - p[1]


# fonction qui permet de departeger deux coups qui ont la même note mais pas très impactante.
def coupsValides(jeu):
	if jeu[1] != moi :
		changeJ(jeu)
		coup = len(getcoups(jeu))
		changeJ(jeu)
	else:
		coup = len(getcoups(jeu))
	return coup

def coupsValidesAdv(jeu):
	if jeu[1] == moi :
		changeJ(jeu)
		coup = len(getcoups(jeu))
		changeJ(jeu)
	else:
		coup = len(getcoups(jeu))
	return coup

def deltaCoupsValides(jeu):
	return coupsValides(jeu) - coupsValidesAdv(jeu)
