#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
import random
import time
import math
infinity = math.inf

global moi
phase1 = [0, 1, 0]
phase2 = [0, 1, 0]
phase3 = [0, 1, 0]
coeffs = [phase1, phase2, phase3]
profondeur = 3
#totaltime = 0
#globaltime = 0

finjeu = game.finJeu
getgagnant = game.getGagnant
getcoups = game.getCoupsValides
changeJ = game.changeJoueur


def saisieCoup(jeu):
	global moi#, totaltime, globaltime
	moi = game.getJoueur(jeu)
	#totaltime = 0

	listeCoupsValides = getcoups(jeu)
	bestCoup = listeCoupsValides[0]
	alpha = -infinity
	beta = infinity
	note = -infinity


	for coup in listeCoupsValides:
		infosJoueCoup = joueCoup(jeu, coup)
		#false = min
		note = estimation(jeu, False, alpha, beta) #MinOuMax bien  egal à False car ce sont des noeud adverse (une bouble deja dans decision)
		annuleCoup(jeu, infosJoueCoup)
		if note > alpha :
			alpha = note
			bestCoup = coup
	#print("TOTALTIME:", totaltime)
	#globaltime += totaltime
	#print("-- GLOBALTIME:", globaltime)
	return bestCoup


def estimation(jeu, MinOuMax, alpha, beta, p = 1):
	#global totaltime

	if finjeu(jeu) : 
	# probleme avec finjeu : on n'annule pas le changement de score en cas de nbcoups > 100...
	# autre probleme : on ajoute à nbCoups à chaque fois que l'on teste un jeu

		g = getgagnant(jeu)

		if (jeu[4][0] + jeu[4][1]) == 48 :
			# On considere que si le score est pile de 48 c'est que nbCoups >= 100
			print("Le nb coups max (100) a probablement été dépassé, on enleve donc les pions qui ont été rajoutés au score dans finJeu")
			for i in jeu[0][0]:
				jeu[4][0] -= i
			for j in jeu[0][1]:
				jeu[4][1] -= j

		if g == moi:
			return 1000
		elif g == moi % 2 + 1 :
			return -1000
		else:
			return -100

	if p >= profondeur :
		return fonctionEvaluation(jeu)
	listeCoupsValides = getcoups(jeu)
	if MinOuMax:
		#MAX
		note = -infinity
		for coup in listeCoupsValides:
			#debut = time.time()
			#prevision = copie(jeu)
			#totaltime += time.time() - debut
			infosJoueCoup = joueCoup(jeu, coup)
			note = max(note, estimation(jeu, not MinOuMax, alpha, beta, p+1))
			#debut = time.time()
			annuleCoup(jeu, infosJoueCoup)
			#totaltime += time.time() - debut
			if note >= beta :
				return note
			alpha = max(alpha, note)
		return note
	else:
		#MIN
		note = infinity
		for coup in listeCoupsValides:
			#debut = time.time()
			#prevision = copie(jeu)
			#totaltime += time.time() - debut
			infosJoueCoup = joueCoup(jeu, coup)
			note = min(note, estimation(jeu, not MinOuMax, alpha, beta, p+1))
			#debut = time.time()
			annuleCoup(jeu,infosJoueCoup)
			#totaltime += time.time() - debut
			if note <= alpha:
				return note
			beta = min(beta, note)
		return note


def fonctionEvaluation(jeu):
	fctsEval = [deltaPlateau(jeu), score(jeu), deltaCoupsValides(jeu)]
	nbPions = jeu[4][0] + jeu[4][1]
	if nbPions <= 12:
		phase = phase1
	elif nbPions <= 48 : 
		phase = phase2
	else : 
		phase = phase3
		
	return  phase[0] * fctsEval[0] + phase[1] * fctsEval[1] + phase[2] * fctsEval[2]

def getCoupsValides(jeu):
	j = getJoueur(jeu)
	a = advEstAffame(jeu)
	return [ ((j-1), i) for i in range(6) if getCaseVal(jeu, j-1, i) > 0 and (not a) or nourrit(jeu,(j-1,i)) ]
	
def advEstAffame(jeu):
	j = getJoueur(jeu)
	adv = j % 2 + 1
	return sum(jeu[0][adv-1]) == 0
	
def joueCoup(jeu, coup):
	#Pour annuleCoup
	affame = False
	nbPionsAEnlever = getCaseVal(jeu, coup[0], coup[1])
	
	l,c = distribue(jeu,coup)
	v = getCaseVal(jeu,l,c)
	j = getJoueur(jeu)
	pris = []
	while (l==(j%2)) and ((v==2 or (v==3))):
		jeu[0][l][c] = 0
		jeu[4][j-1] += v
		pris.append(v)
		l,c = nextCase(l,c,True)
		v = getCaseVal(jeu,l,c)
	if advEstAffame(jeu):
		neMangePas(jeu, pris, l, c)
		jeu[4][j-1] -= sum(pris)
		affame = True 
	jeu[3].append(coup)
	jeu[2] = None
	#l,c = nextCase(l,c)
	changeJ(jeu)
	return (pris, affame, l, c,nbPionsAEnlever)
	
def annuleCoup(jeu, infosJoueCoup):
	j = getJoueur(jeu)
	pris, affame, l, c, nbPionsAEnlever = infosJoueCoup
	if not affame:
		neMangePas(jeu, pris, l, c)
	case = jeu[3].pop()
	nc = case
	l,c = nextCase(l,c)
	jeu[0][case[0]][case[1]] = nbPionsAEnlever
	while nbPionsAEnlever > 0:
		nc = nextCase(nc[0], nc[1])
		if not nc == case:
			jeu[0][nc[0]][nc[1]] -= 1
			nbPionsAEnlever -= 1
	jeu[2] = None
	jeu[4][j%2] -= sum(pris)
	changeJ(jeu)

def advEstAffame(jeu):
	j = getJoueur(jeu)
	adv = j % 2 + 1
	return sum(jeu[0][adv-1]) == 0

def neMangePas(jeu, pris, l, c):
	j = getJoueur(jeu)
	for i in range(len(pris)):
			l,c = nextCase(l,c)
			v = pris[-1-i]
			jeu[0][l][c] += v
			
def distribue(jeu, case):
	v = getCaseVal(jeu, case[0], case[1])
	jeu[0][case[0]][case[1]] = 0
	nc = case
	while v > 0:
		nc = nextCase(nc[0], nc[1])
		if not nc == case:
			jeu[0][nc[0]][nc[1]] += 1
			v -= 1
	return nc
	
def nourrit(jeu, coup):
	j = getJoueur(jeu)
	if j == 1:
		return coup[1] < getCaseVal(jeu, coup[0], coup[1])
	return getCaseVal(jeu, coup[0], coup[1]) > 5-coup[1]
		
		    
def nextCase(l,c,horaire = False):
	if horaire:
		if c == 5 and l == 0:
			return(1,c)
		if c == 0 and l == 1:
			return(0,c)
		if l == 0:
			return(l,c+1)
		return (l, c-1)
	else :
		if c == 0 and l == 0:
			return(1,c)
		if c == 5 and l == 1:
			return(0,c)
		if l == 0:
			return(l,c-1)
		return (l, c+1)

def getCaseVal(jeu, ligne, colonne):
    return jeu[0][ligne][colonne]
   
def getJoueur(jeu):
    return jeu[1]
    
def changeScore(jeu):
	plateau = jeu[0]
	score1 = 0
	score2 = 0
	for ligne in plateau:
		score1 += ligne.count(1)
		score2 += ligne.count(2)
	jeu[4] = (score1, score2)	
		
def score(jeu):
	#print("Note score = ", jeu[4][moi-1] - jeu[4][moi % 2])
	return jeu[4][moi-1] - jeu[4][moi % 2]

def plateau(jeu):
	# JE CONSIDERE QUE NOUS EVALUONS LA NOTE DU JOUEUR DU HAUT
	Map=[[10,  8,  4,  2,  1,  1],
		 [1,  1,  2,  4,  8,  10]]

	noteJeu = 0
	noteJeuAdv = 0
	i2 = 0
	for i in jeu[0][0]:
		noteJeu += Map[0][i2] * getCaseVal(jeu, 0, i2)
		i2 += 1
	i2 = 0
	for i in jeu[0][1]:
		noteJeuAdv += Map[0][i2] * getCaseVal(jeu, 1, i2)
		i2 += 1
	return [noteJeu, noteJeuAdv]

def deltaPlateau(jeu):
	p = plateau(jeu)
	return p[0] - p[1]


# fonction qui permet de departeger deux coups qui ont la même note mais pas très impactante.
def coupsValides(jeu):
	if jeu[1] != moi :
		changeJ(jeu)
		coup = len(getcoups(jeu))
		changeJ(jeu)
	else:
		coup = len(getcoups(jeu))
	return coup

def coupsValidesAdv(jeu):
	if jeu[1] == moi :
		changeJ(jeu)
		coup = len(getcoups(jeu))
		changeJ(jeu)
	else:
		coup = len(getcoups(jeu))
	return coup

def deltaCoupsValides(jeu):
	return coupsValides(jeu) - coupsValidesAdv(jeu)
