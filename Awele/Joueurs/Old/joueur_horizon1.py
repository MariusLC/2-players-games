#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
import random

def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    liste = game.getCoupsValides(jeu)
    coup = horizon1(jeu, liste)
    return coup
    
    
def horizon1(jeu, liste):
	best = 0
	bestCoup = liste[0]
	for coup in liste:
		prevision = game.getCopieJeu(jeu)
		game.joueCoup(prevision, coup)
		# note = a * deltaScore(jeu, prevision) + b deltaPosition ...
		note = 1 * deltaScore(jeu, prevision) + 20 * coin(coup) - 5 * coinAdv(coup) + 3 * bord(coup)
		
		if note > best :
			best = note
			bestCoup = coup
	return bestCoup
	
	
def deltaScore(jeu, prevision):
	j = jeu[1]
	return prevision[4][j-1] - jeu[4][j-1]
	
def coin(coup):
	x, y = coup
	return (x == 0 or x == 7) and (y == 0 or y == 7)
	
def bord(coup):
	x,y = coup
	return (x == 0 or x == 7 or y == 0 or y == 7)
	
def coinAdv(coup):
	x,y = coup
	if x == 0 or x == 7:
		if y == 1 or y == 6:
			return True
	if x == 1 or x == 6:
		if y == 0 or y == 1 or y == 6 or y == 7:
			return True
	return False

