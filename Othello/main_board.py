#!/usr/bin/env python
# -*- coding: utf-8 -*-
import othello
import sys
import time
import random
sys.path.append("..")
import game
game.game = othello
sys.path.append("./Joueurs")
import joueur_humain
import joueur_aleatoire
import joueur_alphabeta
import joueur_alphabeta2
import joueur_site
import joueur_board

game.joueur1 = joueur_aleatoire
game.joueur2 = joueur_aleatoire
joueur1 = "aleatoire"
joueur2 = "board"
hum = False


def selection(joueur):
	global hum
	if (joueur == "humain"):
		newjoueur = joueur_humain
		hum = True
	elif (joueur == "aleatoire"):
		newjoueur = joueur_aleatoire
	elif (joueur == "alphabeta"):
		newjoueur = joueur_alphabeta
	elif (joueur == "alphabeta2"):
		newjoueur = joueur_alphabeta2
	elif (joueur == "board"):
		newjoueur = joueur_board
	elif (joueur == "site"):
		newjoueur = joueur_site

	else:
		print("\nErreur : Nom de joueur inconnu\n")
		exit()
	return newjoueur


nbParties = 100
lenArg = len(sys.argv)
if (lenArg == 1):
	nbApp = 1

elif (lenArg == 2):
	nbApp = int(sys.argv[1])

elif (lenArg == 3):
	nbApp = int(sys.argv[1])
	joueur1 = str(sys.argv[2])
	game.joueur1 = selection(joueur1)

elif (lenArg == 4):
	nbApp = int(sys.argv[1])
	joueur1 = str(sys.argv[2])
	joueur2 = str(sys.argv[3])
	game.joueur1 = selection(joueur1)
	game.joueur2 = selection(joueur2)


def playGame():
	vic = [0, 0, 0]
	for i in range(nbParties):
		jeu = game.initialiseJeu()

		if (nbParties > 1 and i == nbParties//2):
			game.joueur1, game.joueur2 = game.joueur2, game.joueur1

		if hum:
			game.affiche(jeu)

		if not hum:
			jtemp1 = game.joueur1
			jtemp2 = game.joueur2
			game.joueur1 = joueur_aleatoire
			game.joueur2 = joueur_aleatoire
			for j in range(4):
				coup = game.saisieCoup(jeu)
				game.joueCoup(jeu,coup)
			game.joueur1 = jtemp1
			game.joueur2 = jtemp2

		while not(game.finJeu(jeu)):
			copie = game.getCopieJeu(jeu)
			coup = game.saisieCoup(copie)
			game.joueCoup(jeu,coup)
			if hum:
				print("\nJoueur:", jeu[1])
				game.affiche(jeu)

		if (nbParties == 1 or i < nbParties//2):
			vic[game.getGagnant(jeu)] += 1
		else:
			vic[game.getGagnant(jeu)%2+1] += 1

		# nombre de parties jouees
		string = str(i+1) + "/" + str(nbParties)
		# pourcentage de victoire qui s'incrmente
		string += "\t" + str(int((vic[1]*100)/(i+1))) + " %"
		print(string)

	game.joueur1, game.joueur2 = game.joueur2, game.joueur1
	return int(vic[1] * 100 / (nbParties))


def addParam(board, p, k, dep):
	board[p][k] += dep


# Debut de l apprentissage #
t = time.time();
# choix composante
Eps = 10

board = game.joueur1.boardTemp
string_debut = "\n --- DEBUT: " + str(board)
print(string_debut)

result = playGame()
for a in range(nbApp):
	print("\n -- APPRENTISSAGE " + str(a+1) + "/" + str(nbApp))

	p = random.randint(0,len(board)-1)
	k = random.randint(0,len(board[p])-1)
	print("\n - COEFFICIENT " + str(p+1) + "/" + str(k+1) + "\n")

	dep = Eps
	x = random.random()
	if x < 0.5:
		dep *= -1

	addParam(board, p, k, dep)
	result2 = playGame()

	print("\n" + str(result) + " -> " + str(result2) + "\n")
	string_fin = "board = " + str(board)
	if result > result2:
		addParam(board, p, k, -dep)
	else:
		result = result2
		print("board: " + str(board) + "\n")

		
		# creation du fichier
		filename = "Joueurs/coefficients_board.py"
		file = open(filename, "w")
		file.write(string_fin)
		file.close()

	#Eps *= 0.99
	# uniquement si on test plusieurs fois le même coefficient

	if a%100 == 0 :
		# append au fichier de sauvegarde
		filename_bu = "Joueurs/coefficients_backup_board.py"
		file_bu = open(filename_bu, "a")
		file_bu.write(string_fin)
		file_bu.close()

	if (result2 == 100):
		print("BREAK HERE")

print("BOARD: " + str(board) + "\n")
print("\n" + str(a+1) + " apprentissages")
temps = time.time() - t
print("\nTemps d'exécution: " + str(round(temps)) + "s")
print()

# append au fichier de sauvegarde
filename_bu = "Joueurs/coefficients_backup_board.py"
file_bu = open(filename_bu, "a")
file_bu.write(string_fin + "\n")
file_bu.close()
