#!/usr/bin/env python
# -*- coding: utf-8 -*-
import othello
import sys
import time
import random
sys.path.append("..")
import game
game.game = othello
sys.path.append("./Joueurs")
import joueur_humain
import joueur_aleatoire
import joueur_alphabeta
import joueur_alphabeta2
import joueur_site
import joueur_board

game.joueur1 = joueur_aleatoire
game.joueur2 = joueur_aleatoire
joueur1 = "aleatoire"
joueur2 = "alphabeta"
hum = False


def selection(joueur):
	global hum
	if (joueur == "humain"):
		newjoueur = joueur_humain
		hum = True
	elif (joueur == "aleatoire"):
		newjoueur = joueur_aleatoire
	elif (joueur == "alphabeta"):
		newjoueur = joueur_alphabeta
	elif (joueur == "alphabeta2"):
		newjoueur = joueur_alphabeta2
	elif (joueur == "board"):
		newjoueur = joueur_board
	elif (joueur == "site"):
		newjoueur = joueur_site

	else:
		print("\nErreur : Nom de joueur inconnu\n")
		exit()
	return newjoueur


nbParties = 100
lenArg = len(sys.argv)
if (lenArg == 1):
	nbApp = 1

elif (lenArg == 2):
	nbApp = int(sys.argv[1])

elif (lenArg == 3):
	nbApp = int(sys.argv[1])
	joueur1 = str(sys.argv[2])
	game.joueur1 = selection(joueur1)

elif (lenArg == 4):
	nbApp = int(sys.argv[1])
	joueur1 = str(sys.argv[2])
	joueur2 = str(sys.argv[3])
	game.joueur1 = selection(joueur1)
	game.joueur2 = selection(joueur2)


def playGame():
	vic = [0, 0, 0]
	for i in range(nbParties):
		jeu = game.initialiseJeu()

		if (nbParties > 1 and i == nbParties//2):
			game.joueur1, game.joueur2 = game.joueur2, game.joueur1

		if hum:
			game.affiche(jeu)

		if not hum:
			jtemp1 = game.joueur1
			jtemp2 = game.joueur2
			game.joueur1 = joueur_aleatoire
			game.joueur2 = joueur_aleatoire
			for j in range(4):
				coup = game.saisieCoup(jeu)
				game.joueCoup(jeu,coup)
			game.joueur1 = jtemp1
			game.joueur2 = jtemp2

		while not(game.finJeu(jeu)):
			copie = game.getCopieJeu(jeu)
			coup = game.saisieCoup(copie)
			game.joueCoup(jeu,coup)
			if hum:
				print("\nJoueur:", jeu[1])
				game.affiche(jeu)

		if (nbParties == 1 or i < nbParties//2):
			vic[game.getGagnant(jeu)] += 1
		else:
			vic[game.getGagnant(jeu)%2+1] += 1

		# nombre de parties jouees
		string = str(i+1) + "/" + str(nbParties)
		# pourcentage de victoire qui s'incrmente
		string += "\t" + str(int((vic[1]*100)/(i+1))) + " %"
		print(string)

	game.joueur1, game.joueur2 = game.joueur2, game.joueur1
	return int(vic[1] * 100 / (nbParties))


def addParam(phase, p, dep):
	phase[p] += dep


# Debut de l apprentissage #
t = time.time();
# choix composante
Eps = 1

phase1 = game.joueur1.phase1
phase2 = game.joueur1.phase2
phase3 = game.joueur1.phase3
phaseList = [phase1, phase2, phase3]

string_debut = "\n --- DEBUT: " + str(phaseList)
print(string_debut)

result = playGame()
# On compte le nb de victoire d'affilées d'une phase donnée, s'il fait 10 win, on recalcule son % de vic.
win = 0
for a in range(nbApp):
	print("\n -- APPRENTISSAGE " + str(a+1) + "/" + str(nbApp))
	i = random.randint(0,2)
	phase = phaseList[i]
	print("\n - PHASE " + str(i+1) + "/" + str(len(phaseList)))

	p = random.randint(0,len(phase)-1)
	print("\n - COEFFICIENT " + str(p+1) + "/" + str(len(phase)) + "\n")

	dep = Eps
	x = random.random()
	if x < 0.5:
		dep *= -1

	addParam(phase, p, dep)
	result2 = playGame()

	print("\n" + str(result) + " -> " + str(result2) + "\n")
	str1 = "phase1 = " + str(phaseList[0])
	str2 = "phase2 = " + str(phaseList[1])
	str3 = "phase3 = " + str(phaseList[2])
	if result > result2:
		win+=1
		addParam(phase, p, -dep)
	else:
		result = result2
		print("PHASE: " + str(phase) + "\n")

		# string_fin = "phase" + i + " = " + str(phase)
		# creation du fichier
		filename = "Joueurs/coefficients.py"
		file = open(filename, "w")
		file.write(str1 + "\n" + str2 + "\n" + str3)
		file.close()

	#Eps *= 0.999
	# uniquement si on test plusieurs fois le même coefficient

	# On compte le nb de victoire d'affilées d'une phase donnée, s'il fait 10 win, on recalcule son % de vic.
	if win == 10:
		win = 0
		result = playGame()
		
	if a%100 == 0 :
		# append au fichier de sauvegarde
		filename_bu = "Joueurs/coefficients_backup.py"
		file_bu = open(filename_bu, "a")
		file_bu.write(str1 + "\n" + str2 + "\n" + str3 + "\n\n")
		file_bu.close()

	if (result2 == 100):
		print("BREAK HERE")

print("PHASE: " + str(phase) + "\n")
print("\n" + str(a+1) + " apprentissages")
temps = time.time() - t
print("\nTemps d'exécution: " + str(round(temps)) + "s")
print()

# append au fichier de sauvegarde
filename_bu = "Joueurs/coefficients_backup.py"
file_bu = open(filename_bu, "a")
file_bu.write(str1 + "\n" + str2 + "\n" + str3)
file_bu.close()
