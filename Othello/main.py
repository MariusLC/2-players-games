#!/usr/bin/env python
# -*- coding: utf-8 -*-
import othello
import sys
import time
sys.path.append("..")
import game
game.game=othello
sys.path.append("./Joueurs")

import joueur_humain
import joueur_aleatoire
import joueur_alphabeta
import joueur_alphabeta2
import joueur_site
import joueur_board
import Old.joueur_premier_coup

game.joueur1 = joueur_aleatoire
game.joueur2 = joueur_aleatoire
joueur1 = "aleatoire"
joueur2 = "aleatoire"
hum = False


def selection(joueur):
	global hum
	if (joueur == "humain"):
		newjoueur = joueur_humain
		hum = True
	elif (joueur == "aleatoire"):
		newjoueur = joueur_aleatoire
	elif (joueur == "alphabeta"):
		newjoueur = joueur_alphabeta
	elif (joueur == "alphabeta2"):
		newjoueur = joueur_alphabeta2
	elif (joueur == "board"):
		newjoueur = joueur_board
	elif (joueur == "site"):
		newjoueur = joueur_site
	elif (joueur == "premier"):
		newjoueur = Old.joueur_premier_coup

	else:
		print("\nErreur : Nom de joueur inconnu\n")
		exit()
	return newjoueur


lenArg = len(sys.argv)
if (lenArg == 1):
	nbParties = 1

elif (lenArg == 2):
	nbParties = int(sys.argv[1])

elif (lenArg == 3):
	nbParties = int(sys.argv[1])
	joueur1 = str(sys.argv[2])
	game.joueur1 = selection(joueur1)

elif (lenArg == 4):
	nbParties = int(sys.argv[1])
	joueur1 = str(sys.argv[2])
	joueur2 = str(sys.argv[3])
	game.joueur1 = selection(joueur1)
	game.joueur2 = selection(joueur2)

t = time.time();
vic = [0, 0, 0]
for i in range(nbParties):
	debut = time.time();
	jeu = game.initialiseJeu()
	#game.affiche(jeu)
	if (nbParties > 1 and i == nbParties//2):
		game.joueur1, game.joueur2 = game.joueur2, game.joueur1

	if hum:
		game.affiche(jeu)

	if not hum:
		jtemp1 = game.joueur1
		jtemp2 = game.joueur2
		game.joueur1 = joueur_aleatoire
		game.joueur2 = joueur_aleatoire
		for j in range(4):
			coup = game.saisieCoup(jeu)
			game.joueCoup(jeu,coup)
		game.joueur1 = jtemp1
		game.joueur2 = jtemp2

	while not(game.finJeu(jeu)):
		copie = game.getCopieJeu(jeu)
		coup = game.saisieCoup(copie)
		game.joueCoup(jeu,coup)
		if hum:
			print("\nJoueur:", jeu[1])
			game.affiche(jeu)

	if (nbParties == 1 or i < nbParties//2):
		vic[game.getGagnant(jeu)] += 1
	else:
		vic[game.getGagnant(jeu)%2+1] += 1
	
	#game.affiche(jeu)
	print("\nTemps d'exécution partie " + str(i+1) + ": " + str(round(time.time() - debut)) + "s")
	print("Score:", jeu[4])

temps = time.time() - t

vicJ1 = vic[1]
vicJ2 = vic[2]
print("\nNombre de parties:", nbParties)
print("Matchs nuls:", vic[0])
print("Victoires joueur", joueur1, ":", vicJ1, "(" + str(int(vicJ1 * 100 / (nbParties))) + "%)")#, "(" + .. + "/" + ... + ")")
print("Victoires joueur", joueur2, ":", vicJ2, "(" + str(int(vicJ2 * 100 / (nbParties))) + "%)")
print("\nTemps d'exécution: " + str(round(temps)) + "s")
print()
