#!/usr/bin/env python
# -*- coding: utf-8 -*-
import othello
import sys
import time
import random
sys.path.append("..")
import game
game.game=othello
sys.path.append("./Joueurs")
import joueur_humain
import joueur_aleatoire
import joueur_premier_coup
import joueur_dernier_coup
import joueur_horizon1
import joueur_horizon1_1
import joueur_horizon2cp2
import joueur_horizonN
import joueur_horizonN_2
import joueur_horizonNMoy
import joueur_minmax
import joueur_minmax2
import joueur_alphabeta
import joueur_alphabeta2
import joueur_opti
import joueur_opti2
import joueur_copie
import joueur_opti2_copie

game.joueur1=joueur_aleatoire
game.joueur2=joueur_aleatoire
joueur1 = "aleatoire"
joueur2 = "aleatoire"
hum = False


def selection(joueur):
	global hum
	if (joueur == "humain"):
		newjoueur = joueur_humain
		hum = True
	elif (joueur1 == "premier"):
		newjoueur = joueur_premier_coup
	elif (joueur == "dernier"):
		newjoueur = joueur_dernier_coup
	elif (joueur == "horizon1"):
		newjoueur = joueur_horizon1
	elif (joueur == "horizon1_1"):
		newjoueur = joueur_horizon1_1
	elif (joueur == "horizon2"):
		newjoueur = joueur_horizon2cp2
	elif (joueur == "horizonN"):
		newjoueur = joueur_horizonN
	elif (joueur == "horizonNMoy"):
		newjoueur = joueur_horizonNMoy
	elif (joueur == "horizonN_2"):
		newjoueur = joueur_horizonN_2
	elif (joueur == "aleatoire"):
		newjoueur = joueur_aleatoire
	elif (joueur == "minmax"):
		newjoueur = joueur_minmax
	elif (joueur == "minmax2"):
		newjoueur = joueur_minmax2
	elif (joueur == "alphabeta"):
		newjoueur = joueur_alphabeta
	elif (joueur == "alphabeta2"):
		newjoueur = joueur_alphabeta2
	elif (joueur == "opti"):
		newjoueur = joueur_opti
	elif (joueur == "opti2"):
		newjoueur = joueur_opti2
	elif (joueur == "copie"):
		newjoueur = joueur_copie
	elif (joueur == "optic"):
		newjoueur = joueur_opti2_copie
		
	else:
		print("\nErreur : Nom de joueur inconnu\n")
		exit()
	return newjoueur


nbParties = 1 #100
lenArg = len(sys.argv)
if (lenArg == 1):
	nbApp = 1

elif (lenArg == 2):
	nbApp = int(sys.argv[1])

elif (lenArg == 3):
	nbApp = int(sys.argv[1])
	joueur1 = str(sys.argv[2])
	game.joueur1 = selection(joueur1)
	
elif (lenArg == 4):
	nbApp = int(sys.argv[1])
	joueur1 = str(sys.argv[2])
	joueur2 = str(sys.argv[3])
	game.joueur1 = selection(joueur1)
	game.joueur2 = selection(joueur2)


def playGame():
	#t = time.time();
	vic = [0, 0, 0]
	for i in range(nbParties):
		#debut = time.time();
		jeu = game.initialiseJeu()
		#game.affiche(jeu)
		if (nbParties > 1 and i == nbParties//2):
			game.joueur1, game.joueur2 = game.joueur2, game.joueur1

		if hum:
			game.affiche(jeu)

		if not hum:
			jtemp1 = game.joueur1
			jtemp2 = game.joueur2
			game.joueur1 = joueur_aleatoire
			game.joueur2 = joueur_aleatoire
			for j in range(4):
				coup = game.saisieCoup(jeu)
				game.joueCoup(jeu,coup)
			game.joueur1 = jtemp1
			game.joueur2 = jtemp2

		while not(game.finJeu(jeu)):
			copie = game.getCopieJeu(jeu)
			coup = game.saisieCoup(copie)
			game.joueCoup(jeu,coup)
			if hum:
				print("\nJoueur:", jeu[1])
				game.affiche(jeu)

		if (nbParties == 1 or i < nbParties//2):
			vic[game.getGagnant(jeu)] += 1
		else:
			vic[game.getGagnant(jeu)%2+1] += 1
	
		#game.affiche(jeu)
		#print("\nTemps d'exécution partie " + str(i+1) + ": " + str(round(time.time() - debut)) + "s")
		#print("Score:", jeu[4])
		print(str(i+1) + "/" + str(nbParties))

	#temps = time.time() - t

	vicJ1 = vic[1]
	vicJ2 = vic[2]
	'''
	print("\nNombre de parties:", nbParties)
	print("Matchs nuls:", vic[0])
	print("Victoires joueur", joueur1, ":", vicJ1, "(" + str(int(vicJ1 * 100 / (nbParties))) + "%)")#, "(" + .. + "/" + ... + ")")
	print("Victoires joueur", joueur2, ":", vicJ2, "(" + str(int(vicJ2 * 100 / (nbParties))) + "%)")
	print("\nTemps d'exécution: " + str(round(temps)) + "s")
	print()
	'''
	return int(vicJ1 * 100 / (nbParties))
	
	
def addParam(phase, p, dep):
	phase[p] += dep
	


filename = "apprentissage.txt"
file = open(filename, "a")

# choix composante
Eps = 1

phase = game.joueur1.phase1
phaseAdv = game.joueur2.phase1
string_debut = "\n--- DEBUT: " + str(phase) + " - " + str(phaseAdv)
print(string_debut)
file.write(string_debut)

for a in range(nbApp):
	string_app = "\nAPPRENTISSAGE " + str(a+1) + "/" + str(nbApp)
	print(string_app)
	file.write(string_app)

	for p in range(len(phase)):
		result = playGame() #mettre avant le for pour comparer à une valeur de base
	
		dep = Eps
		x = random.random()
		print("\nx:", x, "\n")
		if x < 0.5:
			dep = - Eps
	
		addParam(phase, p, dep)
		result2 = playGame()
	
		print("\n" + str(result) + " -> " + str(result2) + "\n")
		if result > result2:
			addParam(phase, p, -dep)
		else:
			string_phase = "\nPHASE: " + str(phase)
			print(string_phase)
			file.write(string_phase)
		#Eps *= 0.999999

	string_fin = "\n--- FIN: " + str(phase) + " - " + str(phaseAdv) + "\n"
	print(string_fin)
	file.write(string_fin)
file.close()
