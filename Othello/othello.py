#!/usr/bin/env python
# -*- coding: utf-8 -*-

taille = 8

def initJeu():
	return [initPlateau(), 1, None, [], initScore()]

def initPlateau():
	plateau = [[0 for x in range(0,taille,1)] for y in range(0,taille,1)]
	plateau[taille//2 - 1][taille//2 - 1] = 1
	plateau[taille//2][taille//2 - 1] = 2
	plateau[taille//2 - 1][taille//2] = 2
	plateau[taille//2][taille//2] = 1
	return plateau
	
def initScore():
	return (2,2)

def finJeu(jeu):
	score = jeu[4]
	nbCoups = score[0] + score[1]
	return nbCoups >= 64

def getCoupsValides(jeu):
	joueur = jeu[1]
	ListeCoups = []
	plateau = jeu[0]
	joueurAdverse = joueur % 2 + 1

	#on parcours le plateau
	y = 0
	for coupY in plateau:
		x = 0
		for coupX in coupY:
			if coupX == 0:
				#on regarde si un pion adverse est présent sur une case adjacente
				for j in range(y-1, y+2,1):
					for i in range(x-1, x+2,1):
						if (i>=0 and i<taille and j>=0 and j<taille):
							if (plateau[j][i] == joueurAdverse):
								deltaX = i - x
								deltaY = j - y
								newX = x + deltaX
								newY = y + deltaY
								while (((newX>=0) and (newX<taille) and (newY>=0) and (newY<taille) and plateau[newY][newX] == joueurAdverse)):
									newX = newX + deltaX
									newY = newY + deltaY
								if (newX>=0) and (newX<taille) and (newY>=0) and (newY<taille) and plateau[newY][newX] == joueur and not [x,y] in ListeCoups:
									ListeCoups.append([x,y])
			x += 1
		y += 1

	return ListeCoups

def joueCoup(jeu,coup):
	joueur = jeu[1]
	x = coup[0]
	y = coup[1]
	plateau = jeu[0]
	plateau[y][x] = joueur
	joueurAdverse = joueur % 2 + 1
	#on inverse tous les pions alignés à coup
	for j in range(y-1, y+2,1):
		for i in range(x-1, x+2,1):
			if (i>=0 and i<taille and j>=0 and j<taille):
				if (plateau[j][i] == joueurAdverse):
					deltaX = i - x
					deltaY = j - y
					newX = x + deltaX
					newY = y + deltaY
					while ((newX>=0) and (newX<taille) and (newY>=0) and (newY<taille) and (plateau[newY][newX] == joueurAdverse)):
						newX = newX + deltaX
						newY = newY + deltaY
					if (newX>=0) and (newX<taille) and (newY>=0) and (newY<taille) and plateau[newY][newX] == joueur:
						inversePions(jeu,coup,(newX,newY),deltaX,deltaY)
	changeScore(jeu)
	jeu[3].append(coup)

def inversePions(jeu,coup,newCoup,deltaX,deltaY):
	plateau = jeu[0]
	joueur = jeu[1]
	newX = coup[0]
	newY = coup[1]
	while ((not newX == newCoup[0]) or (not newY == newCoup[1])):
		newX = newX + deltaX
		newY = newY + deltaY
		plateau[newY][newX] = joueur
	
def changeScore(jeu):
	plateau = jeu[0]
	score1 = 0
	score2 = 0
	for ligne in plateau:
		score1 += ligne.count(1)
		score2 += ligne.count(2)
	jeu[4] = (score1, score2)
	
	
