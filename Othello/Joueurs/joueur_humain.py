#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game

def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    print("\nSaisie coup:")

    while True:
    	try :
    		coupI = int(input("Abscisse = "))
    		if coupI > 7 or coupI < 0:
    			raise ValueError
    		break
    	except ValueError:
    		print("Oops! choisi un chiffre entre 0 et 7")
    		
    while True:
    	try :
    		coupJ = int(input("Ordonnée = "))
    		if coupJ > 7 or coupJ < 0:
    			raise ValueError
    		break
    	except ValueError:
    		print("Oops! choisi un chiffre entre 0 et 7")
    		
    return [coupI, coupJ]
    
    
