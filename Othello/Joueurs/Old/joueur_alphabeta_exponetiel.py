#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
import random
import time

global moi
global profondeur
profondeur = 2

def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    #debut = time.time();
    global moi
    moi = game.getJoueur(jeu)
    liste = game.getCoupsValides(jeu)
    coup = decision(jeu)
    #fin = time.time();
    #print("Temps pour un coup AB : ", fin-debut, "s")
    return coup



def decision(jeu):
	ListeCoupsValides = game.getCoupsValides(jeu)
	bestCoup = ListeCoupsValides[0]
	Alpha = -100000
	Beta = 100000
	for coup in ListeCoupsValides:
		#debut = time.time();
		prevision = game.getCopieJeu(jeu)
		game.joueCoup(prevision, coup)
		note = estimation(prevision, False, Alpha, Beta) #MinOuMax bien  egal à False car ce sont des noeud adverse (une bouble deja dans decision)
		if note > Alpha :
				Alpha = note
				bestCoup = coup
				if Alpha >= Beta :
					break
	return bestCoup
	
	
def estimation(jeu, MinOuMax, Alpha, Beta, p = 1):
	global profondeur
	global moi
	if game.finJeu(jeu) :
		g = game.getGagnant(jeu)
		if g == moi:
			return 1000
		elif g == moi % 2 + 1 :
			return -1000
		else : 
			return -100
			
	if p == profondeur :
		return fonctionEvaluation(jeu)
	note = None
	ListeCoupsValides = game.getCoupsValides(jeu)
	if MinOuMax :
		for coup in ListeCoupsValides:
			#debut = time.time();
			prevision = game.getCopieJeu(jeu)
			#fin = time.time();
			#print("Temps pour une copie : ", fin-debut, "s")
			game.joueCoup(prevision, coup)
			note = estimation(prevision, not MinOuMax, Alpha, Beta, p+1)
			if note > Alpha :
				Alpha = note
				if Alpha >= Beta :
					break
		return Alpha
	else :
		for coup in ListeCoupsValides:
			#debut = time.time();
			prevision = game.getCopieJeu(jeu)
			#fin = time.time();
			#print("Temps pour une copie : ", fin-debut, "s")
			game.joueCoup(prevision, coup)
			note = estimation(prevision, not MinOuMax, Alpha, Beta, p+1)
			if note < Beta :
				Beta = note
				if Beta <= Alpha :
					break
		return Beta



def fonctionEvaluation(jeu):
	nbPions = jeu[4][0] + jeu[4][1]
	if nbPions <= 12 :
		return  1 * deltaPlateau(jeu) - 3 * score(jeu) + 20 * deltaCoupsValides(jeu)
	elif nbPions <= 48 : 
		return  1 * deltaPlateau(jeu) + 20 * score(jeu)  + 25 *deltaCoupsValides(jeu)
	else : 
		return   1 * score(jeu)  #+ 1 * plateau(jeu) #+ 2 * deltaCoupsValides(jeu)
	
def score(jeu):
	global moi
	#print("Note score = ", jeu[4][moi-1] - jeu[4][moi % 2])
	return jeu[4][moi-1] - jeu[4][moi % 2]
	
def plateau(jeu):
	global moi
	Map=[[100,  -10,  8,   6,   6,   8,  -10, 100],
		 [-10,  -25, -4,  -4,  -4,  -4,  -25, -10],
		 [  8,  -4,   6,   4,   4,   6,  -4,    8],
		 [  6,  -4,   4,   0,   0,   4,  -4,    6],
		 [  6,  -4,   4,   0,   0,   4,  -4,    6],
		 [  8,  -4,   6,   4,   4,   6,  -4,    8],
		 [-10,  -25, -4,  -4,  -4,  -4,  -25, -10],
		 [100,  -10,  8,   6,   6,   8,  -10, 100]]
		 
	noteJeu = 0
	noteJeuAdv = 0
	i2 = 0
	for i in jeu[0]:
		j2 = 0
		for j in i:
			if j == moi:
				noteJeu += Map[i2][j2]
			elif j == moi % 2 + 1:
				noteJeuAdv -= Map[i2][j2]
			j2 += 1
		i2 += 1
	#print(" note jeu : ", noteJeu)
	return [noteJeu, noteJeuAdv]
	
def deltaPlateau(jeu):
	return plateau(jeu)[0] - plateau(jeu)[1]


# fonction qui permet de departeger deux coups qui ont la même note mais pas très impactante.
def coupsValides(jeu):
	if jeu[1] != moi :
		game.changeJoueur(jeu)
	return len(game.getCoupsValides(jeu))
	
def coupsValidesAdv(jeu):
	if jeu[1] == moi :
		game.changeJoueur(jeu)
	return len(game.getCoupsValides(jeu))

def deltaCoupsValides(jeu):
	return coupsValides(jeu) - coupsValidesAdv(jeu)










