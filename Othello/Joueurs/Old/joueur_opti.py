#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
import random
import time
import math
infinity = math.inf

import coefficients

taille = 8
global moi

# au cas ou le fichier n'est pas au bon format on attribue une valeur par defaut
try:
	phase1 = coefficients.phase1
except AttributeError:
	phase1 = [12, -1, 9]

phase2 = phase1
phase3 = phase1

# phase2 = [3, 5, 3]
# phase3 = [0, 1, 0]
#phase1 = [1, 10, 5]
#phase2 = [5, 5, 1] #001 -> 18% win
#phase3 = [0, 1, 0] #001 -> 10% win
coeffs = [phase1, phase2, phase3]
profondeur = 2
#totaltime = 0
#globaltime = 0
pid = 1

finjeu = game.finJeu
getgagnant = game.getGagnant
getcoups = game.getCoupsValides
changeJ = game.changeJoueur


def saisieCoup(jeu):
	global moi#, totaltime, globaltime
	moi = game.getJoueur(jeu)
	#totaltime = 0

	listeCoupsValides = getcoups(jeu)
	bestCoup = listeCoupsValides[0]
	alpha = -infinity
	beta = infinity
	note = -infinity

	#game.affiche(jeu)
	#print(listeCoupsValides)
	for coup in listeCoupsValides:
		listeCoups = joueCoup(jeu, coup)
		#false = min
		note = estimation(jeu, False, alpha, beta) #MinOuMax bien  egal à False car ce sont des noeud adverse (une bouble deja dans decision)
		annuleCoup(jeu, listeCoups)
		if note > alpha :
			alpha = note
			bestCoup = coup
	#print("TOTALTIME:", totaltime)
	#globaltime += totaltime
	#print("-- GLOBALTIME:", globaltime)
	
	return bestCoup


def estimation(jeu, MinOuMax, alpha, beta, p = 1):
	#global totaltime
	global pid
	#print("profondeur =", p, ", pid =", pid)
	pid += 1
	if finjeu(jeu):
		g = getgagnant(jeu)
		if g == moi:
			return 1000
		elif g == moi % 2 + 1:
			return -1000
		else:
			return -100

	if p >= profondeur:
		return fonctionEvaluation(jeu)
	listeCoupsValides = getcoups(jeu)
	if MinOuMax:
		#MAX
		note = -infinity
		for coup in listeCoupsValides:
			#debut = time.time()
			#prevision = copie(jeu)
			#totaltime += time.time() - debut

			ListeCoups = joueCoup(jeu, coup)
			note = max(note, estimation(jeu, not MinOuMax, alpha, beta, p+1))
			#debut = time.time()
			annuleCoup(jeu,ListeCoups)
			#totaltime += time.time() - debut
			if note >= beta:
				return note
			alpha = max(alpha, note)
		return note
	else:
		#MIN
		note = infinity
		for coup in listeCoupsValides:
			#debut = time.time()
			#prevision = copie(jeu)
			#totaltime += time.time() - debut
			ListeCoups = joueCoup(jeu, coup)
			note = min(note, estimation(jeu, not MinOuMax, alpha, beta, p+1))
			#debut = time.time()
			annuleCoup(jeu,ListeCoups)
			#totaltime += time.time() - debut
			if note <= alpha:
				return note
			beta = min(beta, note)
		return note


def fonctionEvaluation(jeu):
	fctsEval = [deltaPlateau(jeu), score(jeu), deltaCoupsValides(jeu)]
	nbPions = jeu[4][0] + jeu[4][1]
	'''
	if nbPions <= 12:
		phase = phase1
	elif nbPions <= 48: 
		phase = phase2
	else : 
		phase = phase3
	'''
	phase = phase1
	fnEval = phase[0] * fctsEval[0] + phase[1] * fctsEval[1] + phase[2] * fctsEval[2]
	return fnEval

def joueCoup(jeu,coup):
	joueur = jeu[1]
	x = coup[0]
	y = coup[1]
	plateau = jeu[0]
	plateau[y][x] = joueur
	joueurAdverse = joueur % 2 + 1
	listePionsRetournes = [coup]
	#on inverse tous les pions alignés à coup
	for j in range(y-1, y+2,1):
		for i in range(x-1, x+2,1):
			if (i>=0 and i<taille and j>=0 and j<taille):
				if (plateau[j][i] == joueurAdverse):
					deltaX = i - x
					deltaY = j - y
					newX = x + deltaX
					newY = y + deltaY
					listePionsDirection = []
					while ((newX>=0) and (newX<taille) and (newY>=0) and (newY<taille) and (plateau[newY][newX] == joueurAdverse)):
						listePionsDirection.append([newX,newY])
						newX += deltaX
						newY += deltaY
					if (newX>=0) and (newX<taille) and (newY>=0) and (newY<taille) and plateau[newY][newX] == joueur:
						listePionsRetournes.extend(listePionsDirection)
	inversePions(jeu, listePionsRetournes)
	jeu[3].append(coup)
	changeScore(jeu)
	changeJ(jeu)
	jeu[2] = None
	return listePionsRetournes

def annuleCoup(jeu,liste):
	plateau = jeu[0]
	coup = liste[0]
	plateau[coup[1]][coup[0]] = 0
	jeu[3].pop()
	liste.pop(0)
	inversePions(jeu,liste)
	changeJ(jeu)
	changeScore(jeu)
	jeu[2] = None

def inversePions(jeu, liste):
	# On a deja changé de joueur dans joueCoup appelée précédemment, "joueur" est donc bien l'adversaire.
	plateau = jeu[0]
	joueur = jeu[1]
	for coup in liste:
		plateau[coup[1]][coup[0]] = joueur

def changeScore(jeu):
	plateau = jeu[0]
	score1 = 0
	score2 = 0
	for ligne in plateau:
		score1 += ligne.count(1)
		score2 += ligne.count(2)
	jeu[4] = (score1, score2)	
		
def score(jeu):
	#print("Note score = ", jeu[4][moi-1] - jeu[4][moi % 2])
	return jeu[4][moi-1] - jeu[4][moi % 2]

def plateau(jeu):
	Map=[[100,  -10,  8,   6,   6,   8,  -10, 100],
		 [-10,  -25, -4,  -4,  -4,  -4,  -25, -10],
		 [  8,  -4,   6,   4,   4,   6,  -4,    8],
		 [  6,  -4,   4,   0,   0,   4,  -4,    6],
		 [  6,  -4,   4,   0,   0,   4,  -4,    6],
		 [  8,  -4,   6,   4,   4,   6,  -4,    8],
		 [-10,  -25, -4,  -4,  -4,  -4,  -25, -10],
		 [100,  -10,  8,   6,   6,   8,  -10, 100]]
	# mettre des types de cases, et multiplier le nombre de cases du type par un coefficient

	noteJeu = 0
	noteJeuAdv = 0
	i2 = 0
	for i in jeu[0]:
		j2 = 0
		for j in i:
			if j == moi:
				noteJeu += Map[i2][j2]
			elif j == moi % 2 + 1:
				noteJeuAdv -= Map[i2][j2]
			j2 += 1
		i2 += 1
	return [noteJeu, noteJeuAdv]

def deltaPlateau(jeu):
	p = plateau(jeu)
	#print("\n\nSCORE PLATEAU:", p[0] - p[1])
	return p[0] - p[1]


# fonction qui permet de departeger deux coups qui ont la même note mais pas très impactante.
def coupsValides(jeu):
	if jeu[1] != moi:
		changeJ(jeu)
		coup = len(getcoups(jeu))
		changeJ(jeu)
	else:
		coup = len(getcoups(jeu))
	return coup

def coupsValidesAdv(jeu):
	if jeu[1] == moi:
		changeJ(jeu)
		coup = len(getcoups(jeu))
		changeJ(jeu)
	else:
		coup = len(getcoups(jeu))
	return coup

def deltaCoupsValides(jeu):
	#print("\n\nDELTA VALIDE:", coupsValides(jeu) - coupsValidesAdv(jeu))
	return coupsValides(jeu) - coupsValidesAdv(jeu)

