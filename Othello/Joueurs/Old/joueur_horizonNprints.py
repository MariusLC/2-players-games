#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
import random

def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    liste = game.getCoupsValides(jeu)
    coup = f(jeu, liste)
    #print("\n Le coup ", coup, " est joué par H3 \n")
    #game.affiche(jeu)
    return coup
    
"""def horizonN(jeu,liste,n):
	for i in range(n):
		boucle(jeu,liste)"""

def f(jeu,liste):
	coup = horizonN(jeu, jeu, liste, 3, 3, True)
	#print("\n\n\n COUP: ", coup, "\n\n\n")
	return coup
	
	
def horizonN(jeu, plateauActuel, liste, profondeur, niveau, MinOuMax):
    NoteMax = -10000
    NoteMin = 10000
    if len(liste) > 0:
    	bestCoup = liste[0] # il faudrait trouver une vraie valeure par default, car cela pourrait créer des erreurs
    else :
    	if MinOuMax :
    		return -10000
    	else :
    		return 10000
    # MinOuMax est un booléan qui vaut true lorsque le noeud est un Max (à nous) et false sinon (noeud adverse)
    for coup in liste:
    	prevision = game.getCopieJeu(plateauActuel)
    	game.joueCoup(prevision, coup)
    	liste2 = game.getCoupsValides(prevision)
    	
    	if niveau == profondeur: # --- RACINE --- On vient d'être appelé (pour H3)
    		note = horizonN(jeu, prevision, liste2, profondeur, niveau-1, not MinOuMax)
    		print("Note : ", note, " vs Note max actuelle : ", NoteMax, "résultat : ", note > NoteMax)
    		if note > NoteMax :
    			print("bestCoupAvant : ", bestCoup)
    			bestCoup = coup
    			print("bestCoupAprès : ", bestCoup)
    	
    	else : # --- TOUS LES NOEUDS NON RACINE ---
    	
    		if niveau == 1 : # --- FEUILLE --- Noeud juste avant les feuilles, il fonctionne comme un noeud max (s'en est un !!), sauf qu'il ne rappelle pas horizonN.
    			note = fonctionEvaluation(jeu, prevision, coup)
    			print("Note : ", note)
    			# Cela marcherait-il sans la ligne en dessous ? en supprimant le if juste en dessous et le else correspondant au test si c'est une feuille, car les feuilles sont des noeuds max.. 
    			if note > NoteMax : 
    				NoteMax = note
    		
    		else :	# --- NIVEAUX INTERMEDIAIRES MIN OU MAX
    		
    			if not MinOuMax : # --- MIN --- Noeud adverse, il garde la note minimale de ses noeuds fils.
    				note = horizonN(jeu, prevision, liste2, profondeur, niveau-1, not MinOuMax)
    				if note < NoteMin :
    					NoteMin = note
    			
    			if MinOuMax : # --- MAX --- Noeud a nous, il garde la note maximale de ses noeuds fils.
    				note = horizonN(jeu, prevision, liste2, profondeur, niveau-1, not MinOuMax)
    				if note > NoteMax :
    					NoteMax = note
    if niveau == profondeur:
    	return bestCoup
    if not MinOuMax : #fusionner NoteMax et NoteMin ?
    	print("NoteMin : ", NoteMin)
    	return NoteMin
    if MinOuMax :
    	print("NoteMax : ", NoteMax)
    	return NoteMax	
		


def fonctionEvaluation(jeu, prevision, coup):
	return 	1 * deltaScore(jeu, prevision) + 20 * coin(coup) - 5 * coinAdv(coup) + 3 * bord(coup)
	
	
def deltaScore(jeu, prevision):
	j = jeu[1]
	return prevision[4][j-1] - jeu[4][j-1]
	
def coin(coup):
	x, y = coup
	return (x == 0 or x == 7) and (y == 0 or y == 7)

# meilleur: 3, mais 2 et 4 sont proches
def bord(coup):
	x,y = coup
	return (x == 0 or x == 7 or y == 0 or y == 7)
	
def coinAdv(coup):
	x,y = coup
	if x == 0 or x == 7:
		if y == 1 or y == 6:
			return True
	if x == 1 or x == 6:
		if y == 0 or y == 1 or y == 6 or y == 7:
			return True
	return False

# def bordAdv(coup)

"""
def longeurLignePions(jeu, coup):
		
		for i in [-1, 0, 1]:
			for j in [-1, 0, 1]:
				
				if
"""	
# fonction qui permet de departeger deux coups qui ont la même note mais pas très impactante.
def deltaCoupsValides(jeu, prevision):
	coupsOld = game.getCoupsValides(jeu)
	coupsNew = game.getCoupsValides(prevision)
	return len(coupsNew) - len(coupsOld)
	
def deltaCoupsValidesAdv(jeu, prevision):
	game.changeJoueur(jeu)
	game.changeJoueur(prevision)
	coupsOld = game.getCoupsValides(jeu)
	coupsNew = game.getCoupsValides(prevision)
	
	game.changeJoueur(jeu)
	game.changeJoueur(prevision)
	return len(coupsNew) - len(coupsOld)




"""
	 * Returns the number of legal moves available to the player about to move
	 * minus the number of legal moves available to the other player
	 */

def map(jeu, prevision, coup):
	Map=[[100,  -10,  8,   6,   6,   8,  -10, 100],
		 [-10,  -25, -4,  -4,  -4,  -4,  -25, -10],
		 [  8,  -4,   6,   4,   4,   6,  -4,    8],
		 [  6,  -4,   4,   0,   0,   4,  -4,    6],
		 [  6,  -4,   4,   0,   0,   4,  -4,    6],
		 [  8,  -4,   6,   4,   4,   6,  -4,    8],
		 [-10,  -25, -4,  -4,  -4,  -4,  -25, -10],
		 [100,  -10,  8,   6,   6,   8,  -10, 100]]
	
	# Si on veut juste la note du coup qu'on joue (H1 donc)
	return Map[coup[1]][coup[0]]
	
	# Si on veut la différence de score (map) entre nos pions sur notre plateau actuel et ceux sur le plateau d'origine 
	NoteJeuOrigine = 0
	for i in jeu[0]:
		for j in i:
			if j == jeu[1]:
				NoteJeuOrigine += Map[i][j]
	NotePrevision = 0
	for i in prevision[0]:
		for j in i:
			if j == prevision[1]:
				NotePrevision += Map[i][j]
	return NotePrevision - NoteJeuOrigine
	
"""











