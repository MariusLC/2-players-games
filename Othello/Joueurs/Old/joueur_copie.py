#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
import random
import time
import math
infinity = math.inf

taille = 8
global moi
phase1 = [10, 1, 10]
phase2 = [3, 5, 3]
phase3 = [0, 1, 0]
coeffs = [phase1, phase2, phase3]
profondeur = 2
#totaltime = 0
#globaltime = 0
pid = 1

finjeu = game.finJeu
getgagnant = game.getGagnant
getcoups = game.getCoupsValides
changeJ = game.changeJoueur
joueCoup = game.joueCoup
copie = game.getCopieJeu

def saisieCoup(jeu):
	global moi#, totaltime, globaltime
	moi = game.getJoueur(jeu)
	#totaltime = 0

	listeCoupsValides = getcoups(jeu)
	bestCoup = listeCoupsValides[0]
	alpha = -infinity
	beta = infinity
	note = -infinity
	
	
	#game.affiche(jeu)
	#print(listeCoupsValides)
	for coup in listeCoupsValides:
		#print(coup)
		prevision = copie(jeu)
		joueCoup(prevision, coup)
		#false = min
		note = estimation(prevision, False, alpha, beta) #MinOuMax bien  egal à False car ce sont des noeud adverse (une bouble deja dans decision)
		if note > alpha :
			alpha = note
			bestCoup = coup
	#print("TOTALTIME:", totaltime)
	#globaltime += totaltime
	#print("-- GLOBALTIME:", globaltime)
	
	return bestCoup


def estimation(jeu, MinOuMax, alpha, beta, p = 1):
	#global totaltime
	global pid
	#print("profondeur =", p, ", pid =", pid)
	pid += 1

	if finjeu(jeu):
		g = getgagnant(jeu)
		if g == moi:
			return 10000000
		elif g == moi % 2 + 1 :
			return -10000000
		else:
			return -10000

	if p == profondeur:
		#print("FEUILLE")
		return fonctionEvaluation(jeu)
	listeCoupsValides = getcoups(jeu)
	if MinOuMax:
		#MAX
		note = -infinity
		for coup in listeCoupsValides:
			#debut = time.time()
			prevision = copie(jeu)
			#totaltime += time.time() - debut
			joueCoup(prevision, coup)
			note = max(note, estimation(prevision, not MinOuMax, alpha, beta, p+1))
			#debut = time.time()
			#totaltime += time.time() - debut
			if note >= beta :
				return note
			alpha = max(alpha, note)
		return note
	else:
		#MIN
		note = infinity
		for coup in listeCoupsValides:
			#debut = time.time()
			prevision = copie(jeu)
			#totaltime += time.time() - debut
			joueCoup(prevision, coup)
			note = min(note, estimation(prevision, not MinOuMax, alpha, beta, p+1))
			#debut = time.time()
			#totaltime += time.time() - debut
			if note <= alpha:
				return note
			beta = min(beta, note)
		return note


def fonctionEvaluation(jeu):
	fctsEval = [deltaPlateau(jeu), score(jeu), deltaCoupsValides(jeu)]
	print(fctsEval)
	nbPions = jeu[4][0] + jeu[4][1]
	if nbPions <= 12:
		phase = phase1
	elif nbPions <= 48 : 
		phase = phase2
	else : 
		phase = phase3
		
	return  phase[0] * fctsEval[0] + phase[1] * fctsEval[1] + phase[2] * fctsEval[2]
		
def score(jeu):
	#print("Note score = ", jeu[4][moi-1] - jeu[4][moi % 2])
	scoreMoi = jeu[4][moi-1]
	scoreAdv = jeu[4][moi % 2]
	nbPionsPoses = scoreMoi + scoreAdv
	return (scoreMoi - scoreAdv)*nbPionsPoses*nbPionsPoses*nbPionsPoses

def plateau(jeu):
	Map=[[100,  -10,  8,   6,   6,   8,  -10, 100],
		 [-10,  -25, -4,  -4,  -4,  -4,  -25, -10],
		 [  8,  -4,   6,   4,   4,   6,  -4,    8],
		 [  6,  -4,   4,   0,   0,   4,  -4,    6],
		 [  6,  -4,   4,   0,   0,   4,  -4,    6],
		 [  8,  -4,   6,   4,   4,   6,  -4,    8],
		 [-10,  -25, -4,  -4,  -4,  -4,  -25, -10],
		 [100,  -10,  8,   6,   6,   8,  -10, 100]]

	noteJeu = 0
	noteJeuAdv = 0
	i2 = 0
	for i in jeu[0]:
		j2 = 0
		for j in i:
			if j == moi:
				noteJeu += Map[i2][j2]
			elif j == moi % 2 + 1:
				noteJeuAdv -= Map[i2][j2]
			j2 += 1
		i2 += 1
	return [noteJeu, noteJeuAdv]

def deltaPlateau(jeu):
	p = plateau(jeu)
	return (p[0] - p[1])*1000


# fonction qui permet de departeger deux coups qui ont la même note mais pas très impactante.
def coupsValides(jeu):
	if jeu[1] != moi :
		changeJ(jeu)
	return len(getcoups(jeu))

def coupsValidesAdv(jeu):
	if jeu[1] == moi :
		changeJ(jeu)
	return len(getcoups(jeu))

def deltaCoupsValides(jeu):
	return coupsValides(jeu) - coupsValidesAdv(jeu)
