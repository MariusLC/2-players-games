"""
cette version d'alpha beta : score 2 et emplacement 3 et mobility
pimpykiwi sur le site
"""

# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game


tabVal = [[10000, -3000, 1000, 800 , 800 , 1000, -3000, 10000],
          [-3000, -5000, -450, -500, -500, -450, -5000, -3000],
          [1000 , -450 , 30  , 10  , 10  , 30  , -450 , 1000 ],
          [800  , -500 , 10  , 50  , 50  , 10  , -500 , 800  ],
          [800  , -500 , 10  , 50  , 50  , 10  , -500 , 800  ],
          [1000 , -450 , 30  , 10  , 10  , 30  , -450 , 1000 ],
          [-3000, -5000, -450, -500, -500, -450, -5000, -3000],
          [10000, -3000, 1000, 800 , 800 , 1000, -3000, 10000]]


pmax=2
cpt=0


def saisieCoup(jeu):
	j=game.getJoueur(jeu)
	coup=decision(jeu,j)
	return coup

def decision(jeu,j): 
	coups=game.getCoupsValides(jeu)
#	global alpha
	alpha=-1000000
#	global beta
	beta=100000
	es=0
	meilleurcoup=coups[0]
	maxEstimation=-100000
	for essai in coups :
		copie=game.getCopieJeu(jeu)
		game.joueCoup(copie, essai)
		es=estimation(copie,1,j,maxEstimation,beta)
		#print(es)
		if(es>maxEstimation):
			maxEstimation=es
			meilleurcoup=essai
#	print(meilleurcoup)
#	print(coups)
	return meilleurcoup


# un noeud max change le alpha

def estimation(jeu,p,j,alpha,beta) :
#ne coupe pas pour la première branche tant que alpha et beta pas initialisés
	global pmax
#	global alpha
#	global beta
	if game.finJeu(jeu):
		g=game.getGagnant(jeu)
		#print("rentrer dans fin jeu avec g=",g)
		if g==j:
			return 20000
		else :
			return -20000
	if p>=pmax :
		return evaluation(jeu,j)
	coups = game.getCoupsValides(jeu)
	#cas min
	if p%2==1 :
		#print(jeu[4])
		v=100000
		for c in coups :
			copie=game.getCopieJeu(jeu)
			game.joueCoup(copie,c)
			es=estimation(copie,p+1,j,alpha,beta)
			if(es<v):
				v=es
			if(alpha>=v):
				return v
			if(v<beta):
				beta=v
		return v
	#cas max
	else :
		v=-100000
		for c in coups :
			copie=game.getCopieJeu(jeu)
			game.joueCoup(copie,c)
			es=estimation(copie,p+1,j,alpha,beta)
			if(v<es):
				v=es
			if(beta>=v):
				return v
			if(v>alpha):
				alpha=v
		return v


def evaluation(jeu,j):
	global cpt
	"""
	tester pour score (alphabeta 2)
	1 : gagne 13 5
	10 : gagne 19 1
	100 : perd 14 5

	tester pour mobility(alphabeta 3 contre alphabeta 2)
	1 : perd 11 9
	10 : gagne 13 6
	20 : perd 11 9
	100 : egalite 10 10
	"""
	res = emplacement(jeu,j) + 10*score(jeu,j) + 10 *mobility(jeu,j)
	cpt=cpt+1
	return res


def score(jeu,j):
	#a voir pour comment gérer les coefficients
	res=0
	res = jeu[4][j-1]-jeu[4][j%2] 
	nombre_coups_joues=len(jeu[3])
	#print(res*nombre_coups_joues) environ 1000
	return res*nombre_coups_joues

def emplacement(jeu,j):
	global tabVal
	res=0
	for i in range(8):
		for g in range(8):
			#pour le joeuur
			if jeu[0][i][g]==j:
				res+=tabVal[i][g]
			# pour l'adversaire	
			if jeu[0][i][g]==j%2+1 :
				res-=tabVal[i][g]
				
	#print(res)
	return res

def mobility(jeu,j):
	res=0
	coupPerso=len(game.getCoupsValides(jeu))
	jeu[1]=jeu[1]%2+1
	coupAdv=len(game.getCoupsValides(jeu))
	jeu[1]=jeu[1]%2+1
	res=coupPerso-coupAdv
	#print(res)
	return res



