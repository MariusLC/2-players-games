#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
import random

#int n = 3

def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    liste = game.getCoupsValides(jeu)
    coup = horizonN(jeu, liste, 2)
    return coup
    
"""def horizonN(jeu,liste,n):
	for i in range(n):
		boucle(jeu,liste)"""
	
def horizonN(jeu,liste,n):
	best = 0
	bestCoup = liste[0]
	for coup in liste:
		prevision = game.getCopieJeu(jeu)
		game.joueCoup(prevision, coup)
		liste2 = game.getCoupsValides(prevision)
		if n-1 > 0:	
			res = boucle(prevision,liste2,n-1,best)
		else:
			note = 1 * deltaScore(jeu, prevision) + 20 * coin(coup) - 5 * coinAdv(coup) + 3 * bord(coup)
		# + 0.05 * deltaCoupsValides(jeu, prevision) - 0.05 * deltaCoupsValidesAdv(jeu,prevision) 
			if note > best :
				best = note
				bestCoup = coup
		
		if res > best :
			best = res
			bestCoup = coup
		
	return bestCoup
	
def boucle(jeu,liste,i,best):
	for coup in liste:
		prevision = game.getCopieJeu(jeu)
		game.joueCoup(prevision, coup)
		liste2 = game.getCoupsValides(prevision)
		if i > 0:	
			note = boucle(prevision,liste2,i-1,best)
		else:
			note = 1 * deltaScore(jeu, prevision) + 20 * coin(coup) - 5 * coinAdv(coup) + 3 * bord(coup)
		# + 0.05 * deltaCoupsValides(jeu, prevision) - 0.05 * deltaCoupsValidesAdv(jeu,prevision) 
			if note > best :
				best = note
	return best
	
	
def deltaScore(jeu, prevision):
	j = jeu[1]
	return prevision[4][j-1] - jeu[4][j-1]
	
def coin(coup):
	x, y = coup
	return (x == 0 or x == 7) and (y == 0 or y == 7)

# meilleur: 3, mais 2 et 4 sont proches
def bord(coup):
	x,y = coup
	return (x == 0 or x == 7 or y == 0 or y == 7)
	
def coinAdv(coup):
	x,y = coup
	if x == 0 or x == 7:
		if y == 1 or y == 6:
			return True
	if x == 1 or x == 6:
		if y == 0 or y == 1 or y == 6 or y == 7:
			return True
	return False

# def bordAdv(coup)

"""
def longeurLignePions(jeu, coup):
		
		for i in [-1, 0, 1]:
			for j in [-1, 0, 1]:
				
				if 
"""	
# fonction qui permet de departeger deux coups qui ont la même note mais pas très impactante.
def deltaCoupsValides(jeu, prevision):
	coupsOld = game.getCoupsValides(jeu)
	coupsNew = game.getCoupsValides(prevision)
	return len(coupsNew) - len(coupsOld)
	
def deltaCoupsValidesAdv(jeu, prevision):
	game.changeJoueur(jeu)
	game.changeJoueur(prevision)
	coupsOld = game.getCoupsValides(jeu)
	coupsNew = game.getCoupsValides(prevision)
	
	game.changeJoueur(jeu)
	game.changeJoueur(prevision)
	return len(coupsNew) - len(coupsOld)
