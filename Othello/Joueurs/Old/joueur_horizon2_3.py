#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
import random

#int n = 3

def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    liste = game.getCoupsValides(jeu)
    coup = f(jeu, liste)
    #print("\n Le coup ", coup, " est joué par H3 \n")
    #game.affiche(jeu)
    return coup
    
"""def horizonN(jeu,liste,n):
	for i in range(n):
		boucle(jeu,liste)"""

def f(jeu,liste):
	coup = horizonN(jeu, liste, 2)
	#print("\n\n\n COUP: ", coup, "\n\n\n")
	return coup[0]
	
	
def horizonN(jeu, liste, n):
	#game.affiche(jeu)
	bestCoup = 0
	bestMoy = 0
	bestNote = 0
	res = 0
	#print("\njoueur", jeu[1], ": ", liste)
	for coup in liste:
		prevision = game.getCopieJeu(jeu)
		game.joueCoup(prevision, coup)
		liste2 = game.getCoupsValides(prevision)
		if n-1 > 0:	
			jneg = horizonN(prevision, liste2, n-1)
			
			if (n == 2):
				res += jneg[1] #somme de nos meilleurs coups
			if (n == 3):
				moy = jneg[1]
				if (moy > bestMoy):
					bestCoup = coup
		else:
			note = 1 * deltaScore(jeu, prevision) + 20 * coin(coup) - 5 * coinAdv(coup) + 3 * bord(coup)
			if note > bestNote :
				bestNote = note
			#print("La feuille ", coup," a obtenu un score de :", note)
		# + 0.05 * deltaCoupsValides(jeu, prevision) - 0.05 * deltaCoupsValidesAdv(jeu,prevision) 
			 
	
	if n == 1:
		#print("total feuilles et nbfeuilles :", total, " ", nbfeuilles)
		return [0, bestNote]
	elif n == 2:
		if (len(liste) > 0):
			tot1 = res / len(liste)
		else:
			tot1 = 0
		return [0, tot1]
	"""
	#option
	elif n == 2:
		#comparer noeud precedent
		print("\n----- Ceci est un noeud H1 :", total, " ", nbfeuilles, " ", tot2, " ", nb2)
	else:
		print("else", total, " ", nbfeuilles, " ", tot2, " ", nb2)
	#option
	"""
	return [bestCoup, 0]


	
def deltaScore(jeu, prevision):
	j = jeu[1]
	return prevision[4][j-1] - jeu[4][j-1]
	
def coin(coup):
	x, y = coup
	return (x == 0 or x == 7) and (y == 0 or y == 7)

# meilleur: 3, mais 2 et 4 sont proches
def bord(coup):
	x,y = coup
	return (x == 0 or x == 7 or y == 0 or y == 7)
	
def coinAdv(coup):
	x,y = coup
	if x == 0 or x == 7:
		if y == 1 or y == 6:
			return True
	if x == 1 or x == 6:
		if y == 0 or y == 1 or y == 6 or y == 7:
			return True
	return False

# def bordAdv(coup)

"""
def longeurLignePions(jeu, coup):
		
		for i in [-1, 0, 1]:
			for j in [-1, 0, 1]:
				
				if 
"""	
# fonction qui permet de departeger deux coups qui ont la même note mais pas très impactante.
def deltaCoupsValides(jeu, prevision):
	coupsOld = game.getCoupsValides(jeu)
	coupsNew = game.getCoupsValides(prevision)
	return len(coupsNew) - len(coupsOld)
	
def deltaCoupsValidesAdv(jeu, prevision):
	game.changeJoueur(jeu)
	game.changeJoueur(prevision)
	coupsOld = game.getCoupsValides(jeu)
	coupsNew = game.getCoupsValides(prevision)
	
	game.changeJoueur(jeu)
	game.changeJoueur(prevision)
	return len(coupsNew) - len(coupsOld)
