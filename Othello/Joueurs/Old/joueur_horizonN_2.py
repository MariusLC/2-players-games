#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
import random

def saisieCoup(jeu):
    """ jeu -> coup
        Retourne un coup a jouer
    """
    liste = game.getCoupsValides(jeu)
    coup = f(jeu, liste)
    #print("\n Le coup ", coup, " est joué par H3 \n")
    #game.affiche(jeu)
    return coup
    
"""def horizonN(jeu,liste,n):
	for i in range(n):
		boucle(jeu,liste)"""

def f(jeu,liste):
	coup = horizonN(jeu, jeu, liste, 3, 3, True)
	#print("\n\n\n COUP: ", coup, "\n\n\n")
	return coup
	
	
def horizonN(jeu, plateauActuel, liste, profondeur, niveau, MinOuMax):
	if liste == [] :
		return 0 # il faut trouver une valeur qui n'impacte pas le jeu (0 va surement etre choisi par un min)
	if niveau == 0 :		# --- FEUILLE ---
		note =  fonctionEvaluation(jeu, plateauActuel)
		#print("Je suis une feuille et j'ai donné la note de : ", note, " au coup précédent")
		return note
		
	else :
		DicoCoupsNotes = {}
		for coup in liste:
			#print("niveau = ", niveau, "\nla liste des coups possibles est : ", liste, "\non a choisi ce coup : ", coup)
			prevision = game.getCopieJeu(plateauActuel)
			game.joueCoup(prevision, coup)
			listeCoupsPossibles = game.getCoupsValides(prevision)
			#print("la listeCoupsPossibles de prévision est : ", listeCoupsPossibles)
			note = horizonN(jeu, prevision, listeCoupsPossibles, profondeur, niveau-1, not MinOuMax)
			#print("le coup :", coup, " a obtenu une note de : ", note,"\n")
			DicoCoupsNotes[note] = tuple(coup)
			
		return NoteDuNoeud(DicoCoupsNotes, profondeur == niveau, MinOuMax)
    	
		

def NoteDuNoeud(DicoCoupsNotes, racine, MinOuMax):
	NoteMin = list(DicoCoupsNotes.keys())[0]
	NoteMax = NoteMin
	CoupMax = DicoCoupsNotes.get(NoteMin)
	CoupMin=CoupMax
	for note in DicoCoupsNotes.keys() :
		#print("niveau = ",niveau," le coup : ", DicoCoupsNotes.get(note), " a obtenu une note de : ", note, " vs NoteMin : ", NoteMin)
		coup = DicoCoupsNotes.get(note)
		if note < NoteMin :
			NoteMin = note
			CoupMin = coup
		elif note > NoteMax :
			NoteMax = note
			CoupMax = coup
	if MinOuMax :
		if racine :
			#print("\n\n\n\n ---------Je suis une racine Max et le coup que je retourne est : ", list(CoupMax), "-------\n\n\n")
			return list(CoupMax)
		#print("\n --- Je suis un Max et le meilleur coup : ", bestCoup," a une note de : ", NoteMax, " ---\n")
		else :
			return NoteMax
	else : 
		if racine :
				#print("\n\n\n\n ---------Je suis une racine Min et le coup que je retourne est : ", list(CoupMin), "-------\n\n\n")
			return list(CoupMin)
		else :
			#print("\n --- Je suis un Min et le pire coup : ", CoupMin," a une note de : ", NoteMin, " --- \n")
			return NoteMin

def fonctionEvaluation(jeu, plateauActuel):
	return 	10 * deltaScore(jeu, plateauActuel) #+ 1 * DeltaMap(jeu,plateauActuel)#+ 20 * coin(coup) - 5 * coinAdv(coup) + 3 * bord(coup)
	
	
def deltaScore(jeu, plateauActuel):
	j = jeu[1]
	return plateauActuel[4][j-1] - jeu[4][j-1]
	
def DeltaMap(jeu, plateauActuel):
	Map=[[100,  -10,  8,   6,   6,   8,  -10, 100],
		 [-10,  -25, -4,  -4,  -4,  -4,  -25, -10],
		 [  8,  -4,   6,   4,   4,   6,  -4,    8],
		 [  6,  -4,   4,   0,   0,   4,  -4,    6],
		 [  6,  -4,   4,   0,   0,   4,  -4,    6],
		 [  8,  -4,   6,   4,   4,   6,  -4,    8],
		 [-10,  -25, -4,  -4,  -4,  -4,  -25, -10],
		 [100,  -10,  8,   6,   6,   8,  -10, 100]]
	
	# Si on veut la différence de score (map) entre nos pions sur notre plateau actuel et ceux sur le plateau d'origine 
	NoteJeuOrigine = 0
	i2 = 0
	for i in jeu[0]:
		for j in i:
			if j == jeu[1]:
				#print(" i2 et j :", i2, j)
				NoteJeuOrigine += Map[i2][j]
			elif j == jeu[1] % 2 + 1:
				NoteJeuOrigine -= Map[i2][j]
		i2 += 1
		
	NotePlateauActuel = 0
	i2 = 0
	for i in plateauActuel[0]:
		for j in i:
			if j == plateauActuel[1]:
				#print(" i2 et j :", i2, j)
				NotePlateauActuel += Map[i2][j]
			elif j == jeu[1] % 2 + 1:
				NotePlateauActuel -= Map[i2][j]
		i2 += 1
	return NotePlateauActuel - NoteJeuOrigine
	
	

# fonction qui permet de departeger deux coups qui ont la même note mais pas très impactante.
def deltaCoupsValides(jeu, plateauActuel):
	coupsOld = game.getCoupsValides(jeu)
	coupsNew = game.getCoupsValides(plateauActuel)
	return len(coupsNew) - len(coupsOld)
	
def deltaCoupsValidesAdv(jeu, plateauActuel):
	game.changeJoueur(jeu)
	game.changeJoueur(plateauActuel)
	coupsOld = game.getCoupsValides(jeu)
	coupsNew = game.getCoupsValides(plateauActuel)
	
	game.changeJoueur(jeu)
	game.changeJoueur(plateauActuel)
	return len(coupsNew) - len(coupsOld)

def DeltaMap(jeu, plateauActuel):
	Map=[[100,  -10,  8,   6,   6,   8,  -10, 100],
		 [-10,  -25, -4,  -4,  -4,  -4,  -25, -10],
		 [  8,  -4,   6,   4,   4,   6,  -4,    8],
		 [  6,  -4,   4,   0,   0,   4,  -4,    6],
		 [  6,  -4,   4,   0,   0,   4,  -4,    6],
		 [  8,  -4,   6,   4,   4,   6,  -4,    8],
		 [-10,  -25, -4,  -4,  -4,  -4,  -25, -10],
		 [100,  -10,  8,   6,   6,   8,  -10, 100]]
	
	# Si on veut la différence de score (map) entre nos pions sur notre plateau actuel et ceux sur le plateau d'origine 
	NoteJeuOrigine = 0
	i2 = 0
	for i in jeu[0]:
		for j in i:
			if j == jeu[1]:
				#print(" i2 et j :", i2, j)
				NoteJeuOrigine += Map[i2][j]
		i2 += 1
		
	NotePlateauActuel = 0
	i2 = 0
	for i in plateauActuel[0]:
		for j in i:
			if j == plateauActuel[1]:
				#print(" i2 et j :", i2, j)
				NotePlateauActuel += Map[i2][j]
		i2 += 1
	return NotePlateauActuel - NoteJeuOrigine











