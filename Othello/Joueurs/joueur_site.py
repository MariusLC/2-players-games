#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append("../..")
import game
import random
import time
import math
infinity = math.inf

taille = 8
global moi
phase1 = [0, 3, 3, -1, 0]
phase2 = [-2, 5, -7, -4, 5]
phase3 = [4, 0, -7, 3, 4]
coeffs = [phase1, phase2, phase3]
nbCoeffs = len(phase1)
profondeur = 2
#totaltime = 0
#globaltime = 0
pid = 1

############ CALCUL DE MAP
Map = [[0 for x in range(8)] for y in range(8)]

boardTemp = [[100, -10, 8, 6],[-10, -25, -4, -4], [8, -4, 6, 4], [6, -4, 4, 0]]

ii = 0
for ligne in boardTemp:
	jj = 0
	for coeffBoard in ligne:
		Map[ii][jj] = coeffBoard
		Map[7-ii][jj] = coeffBoard
		Map[ii][7-jj] = coeffBoard
		Map[7-ii][7-jj] = coeffBoard
		jj += 1
	ii += 1
#########################

finjeu = game.finJeu
getgagnant = game.getGagnant
getcoups = game.getCoupsValides
changeJ = game.changeJoueur
copie = game.getCopieJeu
joueCoup = game.joueCoup

def saisieCoup(jeu):
	global moi#, totaltime, globaltime
	moi = game.getJoueur(jeu)
	#totaltime = 0

	listeCoupsValides = getcoups(jeu)
	bestCoup = listeCoupsValides[0]
	alpha = -infinity
	beta = infinity
	note = -infinity
	
	
	#game.affiche(jeu)
	#print(listeCoupsValides)
	for coup in listeCoupsValides:
		#print(coup)
		prevision = copie(jeu)
		joueCoup(prevision, coup)
		#false = min
		note = estimation(prevision, False, alpha, beta) #MinOuMax bien  egal à False car ce sont des noeud adverse (une bouble deja dans decision)
		if note > alpha :
			alpha = note
			bestCoup = coup
	#print("TOTALTIME:", totaltime)
	#globaltime += totaltime
	#print("-- GLOBALTIME:", globaltime)
	
	return bestCoup


def estimation(jeu, MinOuMax, alpha, beta, p = 1):
	#global totaltime
	global pid
	#print("profondeur =", p, ", pid =", pid)
	pid += 1

	if finjeu(jeu):
		g = getgagnant(jeu)
		if g == moi:
			return 10000000
		elif g == moi % 2 + 1 :
			return -10000000
		else:
			return -10000

	if p == profondeur:
		#print("FEUILLE")
		return fonctionEvaluation(jeu)
	listeCoupsValides = getcoups(jeu)
	if MinOuMax:
		#MAX
		note = -infinity
		for coup in listeCoupsValides:
			#debut = time.time()
			prevision = copie(jeu)
			#totaltime += time.time() - debut
			joueCoup(prevision, coup)
			note = max(note, estimation(prevision, not MinOuMax, alpha, beta, p+1))
			#debut = time.time()
			#totaltime += time.time() - debut
			if note >= beta :
				return note
			alpha = max(alpha, note)
		return note
	else:
		#MIN
		note = infinity
		for coup in listeCoupsValides:
			#debut = time.time()
			prevision = copie(jeu)
			#totaltime += time.time() - debut
			joueCoup(prevision, coup)
			note = min(note, estimation(prevision, not MinOuMax, alpha, beta, p+1))
			#debut = time.time()
			#totaltime += time.time() - debut
			if note <= alpha:
				return note
			beta = min(beta, note)
		return note

def inversePions(jeu, liste):
	# On a deja changé de joueur dans joueCoup appelée précédemment, "joueur" est donc bien l'adversaire.
	plateau = jeu[0]
	joueur = jeu[1]
	for coup in liste:
		plateau[coup[1]][coup[0]] = joueur

def changeScore(jeu):
	plateau = jeu[0]
	score1 = 0
	score2 = 0
	for ligne in plateau:
		score1 += ligne.count(1)
		score2 += ligne.count(2)
	jeu[4] = (score1, score2)	

def fonctionEvaluation(jeu):
	res = 0;
	plat = [plateau(jeu, moi), plateau(jeu, moi%2+1)]
	cpValides = [coupsValides(jeu, moi), coupsValides(jeu, moi%2+1)]
	fctsEval = [score(jeu), plat[0], plat[1], cpValides[0], cpValides[1]]
	nbPions = jeu[4][0] + jeu[4][1]

	if nbPions <= 12:
		phase = phase1
	elif nbPions <= 48: 
		phase = phase2
	else : 
		phase = phase3

	if nbCoeffs != len(fctsEval):
		print("erreur : Pas le meme nb coeffs dans phase et fctsEval")

	# phase[0] * fctsEval[0] + phase[1] * fctsEval[1] + phase[2] * fctsEval[2]
	for i in range(len(phase)):
		res += fctsEval[i] * phase[i]
	return res
		
def score(jeu):
	#print("Note score = ", jeu[4][moi-1] - jeu[4][moi % 2])
	return intervalle(jeu[4][moi-1] - jeu[4][moi % 2], 50)

def plateau(jeu, joueur):
	# mettre des types de cases, et multiplier le nombre de cases du type par un coefficient

	note = 0
	i2 = 0
	for i in jeu[0]:
		j2 = 0
		for j in i:
			if j == joueur:
				note += Map[i2][j2]
			j2 += 1
		i2 += 1
	return intervalle(note, 600)

def deltaPlateau(jeu):
	return plateau(jeu, moi) - plateau(jeu, moi%2+1)


# fonction qui permet de departeger deux coups qui ont la même note mais pas très impactante.
def coupsValides(jeu, joueur):
	if jeu[1] != joueur:
		changeJ(jeu)
		coup = len(getcoups(jeu))
		changeJ(jeu)
	else:
		coup = len(getcoups(jeu))
	return intervalle(coup, 30)

# def coupsValides(jeu):
# 	if jeu[1] != moi:
# 		changeJ(jeu)
# 		coup = len(getcoups(jeu))
# 		changeJ(jeu)
# 	else:
# 		coup = len(getcoups(jeu))
# 	return coup

# def coupsValidesAdv(jeu):
# 	if jeu[1] == moi:
# 		changeJ(jeu)
# 		coup = len(getcoups(jeu))
# 		changeJ(jeu)
# 	else:
# 		coup = len(getcoups(jeu))
# 	return coup

def deltaCoupsValides(jeu):
	#print("\n\nDELTA VALIDE:", coupsValides(jeu) - coupsValidesAdv(jeu))
	return coupsValides(jeu) - coupsValidesAdv(jeu)

def intervalle(score, total):
	score = (score / total ) + 0.5
	if score > 1 :
		score = 1
	if score < 0 :
		score = 0
	return score